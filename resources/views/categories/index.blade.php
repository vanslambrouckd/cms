@extends('layouts.admin')


@section('content')
<a href="{{route('products.create')}}">create</a>


@include('partials.success')
<h1 class="ui header">
  Categories
  <div class="sub header">overview</div>
</h1>
custom
<table class="ui celled table">
    <thead>
        <th class="one wide">Id</th>
        <th class="five wide">Title</th>
        <th class="one wide">Action</th>
        <th class="two wide">action</th>
    </thead>
    <tbody>
    @foreach($categories as $item)
    <tr>
        <td>{{$item->id}}</td>
        <td>{{$item->title}}</td>
        <td>
            <a href="{{action('CategoryController@edit', ['id' => $item->id])}}">edit</a>
        </td>
        <td>
            <form method="post" action="{{action('CategoryController@destroy', ['id' => $item->id])}}">
                {{ method_field('DELETE') }}                
                {{csrf_field()}}                
                <button type="submit" name="delete">delete</button>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
@endsection