@extends('layouts.admin')


@section('content')
<div class="page-header clearfix">
    <div class="title pull-left">
        <h1>{{$contentType->get('name')}}</h1>
        <h2>Edit</h2>    
    </div>
    <div class="actions pull-right">
    </div>
</div>

@include('partials.errors')
@include('partials.success')

<div class="row">
    <div class="col-md-2">
    @include('contentitem_menu')
    </div>
    <div class="col-md-10">
      <form method="post" action="{{action('ProductController@specifications_store', ['id' => $product->id])}}" class="form">
        {{csrf_field()}}
        <ul id="specslist">
          @foreach($items_per_tag as $tag)
          <li>
            <span class="specification">{{$tag['name']}}</span>
            <ul>
            @foreach($tag['data'] as $data_item_value => $data_item)
              <li>
                <div class="form-group spec-item">
                  <label>{{$data_item->specification}} ({{$data_item_value}})</label>
                  <input type="text" class="form-control input-sm" name="relation[{{$data_item_value}}][value]" value="{{$values_selected[$data_item_value]->pivot->value or ''}}" />
                </div>
                <hr />
              </li>
            @endforeach
            </ul>
          </li>
          @endforeach
        </ul>
        
        <button class="btn btn-primary" type="submit" name="save">save</button>
      </form>
    </div>
</div>
@endsection
