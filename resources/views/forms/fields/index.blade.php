@extends('layouts.admin')


@section('content')
@include('partials.success')
<div class="page-header clearfix">
    <div class="title pull-left">
        <h1>{{$contentType->get('name')}}</h1>
        <h2>Overview</h2>    
    </div>
    <div class="actions pull-right">
        <a class="btn btn-default btn-add" href="{{route('content.forms.fields.create', ['form' => $form])}}"><i class="fa fa-plus" aria-hidden="true"></i>create field</a>
    </div>
</div>

@include('partials.errors')
@include('partials.success')

<div class="row">
    <div class="col-md-2">
        @include('contentitem_menu')        
    </div>
    <div class="col-md-10">
        <table class="table table-striped table-bordered">
            <thead>
                <th>Id</th>
                <th>Title</th>
                <th>Action</th>
            </thead>
            <tbody>
            @foreach($fields as $field)
            <tr>
                <td>{{$field->id}}</td>
                <td>{{$field->title}}</td>
                <td class="actions">
                    <a class="btn btn-default btn-edit" href="{{action('FormController@fieldsEdit', ['form' => $form, 'field' => $field])}}"></a>
                    <form method="post" action="{{action('FormController@fieldsDestroy', ['form' => $form, 'field' => $field->id])}}">
                        {{ method_field('DELETE') }}                
                        {{csrf_field()}}        
                        <button class="btn btn-danger btn-delete" type="submit" name="delete"></button>        
                    </form>
                </td>
            </tr>
            @endforeach
            </tbody>    
        </table>
    </div>
</div>        
@endsection    