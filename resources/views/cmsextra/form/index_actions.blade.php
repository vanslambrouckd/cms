<td class="actions">
    <a class="btn btn-default btn-edit" href="{{$action_edit}}"></a>
    <form method="post" action="{{$action_delete}}">
        {{ method_field('DELETE') }}                
        {{csrf_field()}}        
        <button class="btn btn-danger btn-delete" type="submit" name="delete"></button>        
    </form>

      <a class="btn btn-default" href="{{action('FormController@preview', ['form' => $item])}}" target="_blank"><i class="fa fa-search" aria-hidden="true"></i></a>
</td>