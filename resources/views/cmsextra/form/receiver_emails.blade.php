<div class="form-group">
  <label for="">{{$field->name}}</label>
  <textarea name="{{$field->field_name}}" class="form-control" {{$str_readonly}}>{{$field->postback_value}}</textarea>    
  <div class="help-block">
      Per regel een email adres, eventueel kommagescheiden met naam
  </div>
</div>