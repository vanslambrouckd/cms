@extends('layouts.admin')


@section('content')
<h1 class="ui header">
  Brands
  <div class="sub header">Overview</div>
</h1>

<a href="{{route('products.create')}}">create</a>

@include('partials.success')

<div class="container">
<table class="ui celled table">
    <thead>
        <th class="one wide">Id</th>
        <th class="five wide">Title</th>
        <th class="one wide">Action</th>
        <th class="two wide">action</th>
    </thead>
    <tbody>
    @foreach($brands as $item)
    <tr>
        <td>{{$item->id}}</td>
        <td>{{$item->title}}</td>
        <td>
            <a class="button" href="{{action('BrandController@edit', ['id' => $item->id])}}"><i class="pencil icon"></i>edit</a>
        </td>
        <td>
            <form method="post" action="{{action('BrandController@destroy', ['id' => $item->id])}}">
                {{ method_field('DELETE') }}                
                {{csrf_field()}}        
                <button class="ui icon button" type="submit" name="delete">
                  <i class="trash icon"></i>
                </button>        
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>    
</table>
@endsection    
</div>
