<ul class="nav nav-pills nav-stacked">
@foreach($menu_items as $menu_item)
    <li role="presentation" class="{{$menu_item->class}}">
        <a class="{{$menu_item->cls_disabled}}" href="{{$menu_item->url}}">{{$menu_item->title}}</a>
    </li>
@endforeach
</ul> 