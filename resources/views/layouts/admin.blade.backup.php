<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.css">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <div class="ui left sidebar inverted vertical menu visible">
        <a class="item" href="{{action('ContentController@index', ['type' => 'products'])}}">
          Products
      </a>
      <a class="item" href="{{action('ContentController@index', ['type' => 'categories'])}}">
          Categories
      </a>
      <a class="item" href="{{action('ContentController@index', ['type' => 'brands'])}}">
          Brands
      </a>       
      <a class="item" href="{{action('ContentController@index', ['type' => 'users'])}}">
          Users
      </a>     
      <a class="item" href="{{action('ContentController@index', ['type' => 'reviews'])}}">
          Reviews
      </a>     
      <a class="item" href="{{action('ContentController@index', ['type' => 'countries'])}}">
          Countries
      </a> 
      <a class="item" href="{{action('ContentController@index', ['type' => 'menu_items'])}}">
          Menu items
      </a>  
      <a class="item" href="{{action('ContentController@index', ['type' => 'tags'])}}">
          Tags
      </a>    
      <a class="item" href="{{action('ContentController@index', ['type' => 'specifications'])}}">
          Specifications
      </a>  
      <a class="item" href="{{action('ContentController@index', ['type' => 'forms'])}}">
          Forms
      </a>    
      <a class="item" href="{{action('CmsBuildController@index')}}">
          Builder
      </a>    
  </div>
  <div class="pusher">
      @yield('content')
  </div>


  @section('js-footer')
    <script src="{{ asset('js/app.js') }}"></script>  
    <script src="{{ asset('js/resources.js') }}"></script>  
  @show
</body>
</html>