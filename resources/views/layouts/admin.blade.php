<!doctype html>
<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <input type="hidden" name="_token" value="{{ csrf_token() }}" >
  <meta name="_token" content="{{ csrf_token() }}">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  
  <!-- <link rel="manifest" href="site.webmanifest"> -->
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">


  <link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}">

  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.1/css/bulma.css"> -->

  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
</head>
<body>
  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Help</a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>
  <div class="container-fluid" style="margin-top: 80px;"> <!-- todo:fix -->
    <div class="row">
      <div class="col-md-2">
        <ul class="nav nav-sidebar">
          <li>
            <a class="item" href="{{action('ContentController@index', ['type' => 'products'])}}">
              Products
            </a>
          </li>
          <li>
            <a class="item" href="{{action('ContentController@index', ['type' => 'categories'])}}">
              Categories
            </a>
          </li>
          <li>
            <a class="item" href="{{action('ContentController@index', ['type' => 'brands'])}}">
              Brands
            </a>
          </li>
          <li>       
            <a class="item" href="{{action('ContentController@index', ['type' => 'users'])}}">
              Users
            </a>
          </li>
          <li>     
            <a class="item" href="{{action('ContentController@index', ['type' => 'reviews'])}}">
              Reviews
            </a>
          </li>
          <li>     
            <a class="item" href="{{action('ContentController@index', ['type' => 'countries'])}}">
              Countries
            </a>
          </li>
          <li> 
            <a class="item" href="{{action('ContentController@index', ['type' => 'menu_items'])}}">
              Menu items
            </a>
          </li>
          <li>  
            <a class="item" href="{{action('ContentController@index', ['type' => 'tags'])}}">
              Tags
            </a>
          </li>
          <li>    
            <a class="item" href="{{action('ContentController@index', ['type' => 'specifications'])}}">
              Specifications
            </a>
          </li>
          <!-- <li>  
            <a class="item" href="{{action('ContentController@index', ['type' => 'forms'])}}">
              Forms
            </a>
          </li> -->
          <li>    
            <a class="item" href="{{action('CmsBuildController@index')}}">
              Builder
            </a>
          </li>
        </ul>
      </div>
      <div class="col-md-10">
        
       <div id="app">
          <modal title="Meta data" :allow_close=true id="modal">   
              <div class="modal-body">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, recusandae quia unde rerum non et doloribus nam architecto repudiandae incidunt commodi pariatur dolores quam praesentium dicta dolorum neque at velit.
              </div>  
          </modal>   

          <modal title="Verwijderen" :allow_close=false id="modal-delete">   
              <form>
                <div class="modal-body">
                  Weet u zeker dat u dit item wilt verwijderen?
                </div>  
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>                
                  <button class="btn btn-danger js-ok">OK</button>
                </div>
              </form>
          </modal>          
       @yield('content')
        
       </div>
     </div>
   </div>
 </div>
 <script src="{{asset('js/modernizr-3.5.0.min.js')}}"></script>
 <!-- <script src="{{asset('js/axios.min.js')}}"></script> -->
 <script src="{{asset('js/app.js')}}"></script>
 <!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script> -->
@section('js-footer')
@show
</body>
</html>
