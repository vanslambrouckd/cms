<!doctype html>
<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">


  <link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}">

  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.1/css/bulma.css"> -->
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
          <![endif]-->

          <!-- Add your site or application content here -->
          <div id="wrapper">
            <div id="nav">
              <ul>
                <li>
                  <a class="item" href="{{action('ContentController@index', ['type' => 'products'])}}">
                    Products
                  </a>
                </li>
                <li>
                  <a class="item" href="{{action('ContentController@index', ['type' => 'categories'])}}">
                    Categories
                  </a>
                </li>
                <li>
                  <a class="item" href="{{action('ContentController@index', ['type' => 'brands'])}}">
                    Brands
                  </a>
                </li>
                <li>       
                  <a class="item" href="{{action('ContentController@index', ['type' => 'users'])}}">
                    Users
                  </a>
                </li>
                <li>     
                  <a class="item" href="{{action('ContentController@index', ['type' => 'reviews'])}}">
                    Reviews
                  </a>
                </li>
                <li>     
                  <a class="item" href="{{action('ContentController@index', ['type' => 'countries'])}}">
                    Countries
                  </a>
                </li>
                <li> 
                  <a class="item" href="{{action('ContentController@index', ['type' => 'menu_items'])}}">
                    Menu items
                  </a>
                </li>
                <li>  
                  <a class="item" href="{{action('ContentController@index', ['type' => 'tags'])}}">
                    Tags
                  </a>
                </li>
                <li>    
                  <a class="item" href="{{action('ContentController@index', ['type' => 'specifications'])}}">
                    Specifications
                  </a>
                </li>
                <li>  
                  <a class="item" href="{{action('ContentController@index', ['type' => 'forms'])}}">
                    Forms
                  </a>
                </li>
                <li>    
                  <a class="item" href="{{action('CmsBuildController@index')}}">
                    Builder
                  </a>
                </li>
              </ul>
            </div>
            <div id="content">
              <div id="app">
              </div>
              @yield('content')
            </div>
          </div>
          <script src="{{asset('js/modernizr-3.5.0.min.js')}}"></script>
          <script src="{{asset('js/app.js')}}"></script>
          <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        </body>
        </html>
