@extends('layouts.admin')


@section('content')
<a href="#">create</a>

@include('partials.success')

<h1 class="ui header">
  Builder
  <div class="sub header">overview</div>
</h1>

custom
<div class="container">
<table class="ui celled table">
    <thead>
        <th>Model</th>
        <th>Action</th>
    </thead>
    <tbody>
    @foreach($files as $file)
    <tr>
        <td>
            {{$file->getFilename()}}
        </td>
        <td>
            <a class="button" href="{{action('CmsBuildController@edit', ['filename' => $file->getFilename()])}}"><i class="pencil icon"></i>edit</a>
            <form method="post" action="#">
                {{ method_field('DELETE') }}                
                {{csrf_field()}}        
          </form>
        </td>
    </tr>
    @endforeach
    </tbody>    
</table>
@endsection    
</div>