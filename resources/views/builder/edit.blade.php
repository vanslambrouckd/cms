
@extends('layouts.admin')


@section('content')
<style>
.col {
  overflow: scroll;
  display:block;
  float: left;
  padding-left: 15px;
  padding-right: 15px;
  padding-bottom: 10px;
  margin-bottom: 10px;
  border-bottom: solid 1px #ddd;
}

.field {
  margin-bottom: 15px;
}

label {
  display: block;
  margin-bottom: 5px;
}

input[type=text], select {
  width:100%;
  padding:5px;
}
</style>

<form method="post" action="{{action('CmsBuildController@store', ['filename' => $filename])}}" class="ui form">
    {{csrf_field()}}
    {{ method_field('POST') }}
<h2>Fields</h2>
<div class="ui grid">
  @foreach($data['fields'] as $key => $field)
  <div class="col one wide">
    <div class="field">
      <label for="">Name</label>
      <input type="text" name="fields[{{$key}}][name]" value="{{$field['name'] or ''}}" />
    </div>

    <div class="field">
      <label for="">Field Name</label>
      <input type="text" name="fields[{{$key}}][field_name]" value="{{$field['field_name'] or ''}}" />
    </div>

    <div class="field">
      <label for="">Field type</label>

      <select name="fields[{{$key}}][field_type]" class="select2">
        <?php
        $field_type = isset($field['field_type'])?$field['field_type']:'';
        ?>
        @foreach($fieldtypes as $val => $type)
        <option value="{{$type}}" @if ($field_type == $type) selected="selected" @endif>{{$type}}</option>
        @endforeach
      </select>
    </div>

    <div class="field">
      <label for="">Render type</label>
      <select name="fields[{{$key}}][render_type]" class="select2">
        <?php
        $render_type = isset($field['render_type'])?$field['render_type']:'';
        ?>
        @foreach($rendertypes as $val => $type)
        <option value="{{$val}}" @if ($render_type == $type) selected="selected" @endif>{{$type}}</option>
        @endforeach
      </select>
    </div>



    <div class="field">
      <label for="">Rules</label>
      <input type="text" name="fields[{{$key}}][rules]" value="{{$field['rules'] or ''}}" />
    </div>

    <div class="field">
      <label for="">Data value</label>
      <input type="text" name="fields[{{$key}}][data_value]" value="{{$field['data_value'] or ''}}" />
    </div>

    <div class="field">
      <label for="">Relation</label>
      <select name="fields[{{$key}}][relation]" class="select2">
        <option></option>
        <?php
        $relation = isset($field['relation'])?$field['relation']:'';
        ?>
        @foreach($relations as $val => $type)
        <option value="{{$type}}" @if ($relation == $type) selected="selected" @endif>{{$type}}</option>
        @endforeach
      </select>
    </div>
    <div class="field">
      <label for="">Heading</label>
      <input type="text" name="fields[{{$key}}][heading]" value="{{$field['heading'] or ''}}" />
    </div>

    <div class="field">
      <label for="">Related class</label>
      <input type="text" name="fields[{{$key}}][related_class]" value="{{$field['related_class'] or ''}}" class="related_class classlist" />
    </div>

    <div class="field">
      <label for="">Related field</label>
      <input type="text" name="fields[{{$key}}][related_field]" value="{{$field['related_field'] or ''}}" />
    </div>

    <div class="field">
      <label for="">Data class</label>
      <input type="text" name="fields[{{$key}}][data_class]" value="{{$field['data_class'] or ''}}" class="data_class classlist" data-key="{{$key}}" id="data_class_{{$key}}" />
    </div>

    <div class="field">
      <label for="">Data function</label>
      <!-- <input type="text" name="fields[{{$key}}][data_function]" value="{{$field['data_function'] or ''}}" class="data_function" /> -->
      <select name="fields[{{$key}}][data_function]" class="data_function" id="data_function_{{$key}}" data-data_function_index="{{$key}}">
      </select>
    </div>

    <div class="field">
      <label for="">Data value</label>
      <input type="text" name="fields[{{$key}}][data_value]" value="{{$field['data_value'] or ''}}" />
    </div>

    <div class="field">
      <label for="">Data label</label>
      <input type="text" name="fields[{{$key}}][data_label]" value="{{$field['data_label'] or ''}}" />
    </div>
  </div>
  @endforeach
</div>
  <button type="submit" name="save">save</button>

  
<h2>Media</h2>

<h2>Tabs</h2>
</form>
@endsection

@section('js-footer')
@parent
<script type="text/javascript">
  $(document).ready(function(){

    $('.classlist').select2({
      width: '100%',
      allowClear: true,
      multiple: false,
      maximumSelectionSize: 1,
      placeholder: "Click here and start typing to search.",
      data: [
      @foreach($data_classes as $type)
      { id: "{{$type}}", text: "{{$type}}"     },
      @endforeach
      ]    
    });

    $('.data_class').each(function(index, val) {
      var key = $(this).attr('data-key');
      populate_data_function(key);
    });

    $('.data_class').on('select2:select', function (e) {
      // Do something
      var key = $(this).attr('data-key');
      populate_data_function(key);
      console.log('jaa');
      $('#data_function_'+key).trigger('change');
    });

    function populate_data_function(column) {
      var data_class_value = $('#data_class_'+column).val();
      var $data_function = $('#data_function_'+column);

      $data_function.select2({
        ajax: {
          url: 'http://cms.dev/admin/builder/get_object_methods',
          data: function (params) {
            var query = {
              search: params.term,
              data_class: data_class_value
            }
          return query;
        },
        tags:true,
        method: 'get',
        dataType: 'json',
        processResults: function (data, params) {
           //data is an array of results
           data.forEach(function (d, i) {
               //Just assign a unique number or your own unique id
               d.id = i; // or e.id = e.userId;
             })

           return {
             results: data
           };
         },
       }
     });
    }
  });
</script>
@endsection