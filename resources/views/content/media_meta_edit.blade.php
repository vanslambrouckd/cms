  {{--
  @if (in_array($mediaItem->mime_type, ['image/jpeg']))
  <img class="ui small image" src="{{$mediaItem->getUrl()}}" style="max-width:100px;" />
  @endif
  --}}

  <form method="post" action="{{action('ContentController@mediaMetaUpdate', ['type' => $contentType->type, 'id' => $item->id, 'collection_name' => $collection_name, 'media_id' => $mediaItem->id])}}" class="form">
          <div class="modal-body">
              <div class="row">
                  <div class="col-md-12">
                      {{csrf_field()}}
                      {{ method_field('PATCH') }}                    
                      <div class="row">   
                          <div class="col-md-12">
                              <!-- -->
                              TODO: REFACTOR!!
                              @foreach($mediaProperties as $property_title => $property)
                                      @if ($property['field_type'] == 'textfield')
                                      <div class="form-group">
                                        <label for="">{{$property_title}}</label>
                                        <input type="text" class="form-control input-sm" name="properties[{{$property_title}}]" value="{{$mediaItem->getCustomProperty($property_title)}}" />    
                                      </div>
                                      @elseif ($property['field_type'] == 'textarea')
                                      <div class="form-group">
                                        <label for="">{{$property_title}}</label>
                                        <textarea class="form-control" rows="3" name="properties[{{$property_title}}]">{{$mediaItem->getCustomProperty($property_title)}}</textarea>          
                                      </div>
                                      @endif
                              @endforeach
                              <!-- -->
                          </div>                
                      </div>
                  </div>
              </div>
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Sluiten</button>
              <button type="submit" class="btn btn-primary" name="save">Opslaan</button>
          </div>
  </form>
