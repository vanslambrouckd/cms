&lt;?php
namespace App;

use Illuminate\Database\Eloquent\Model;
@if ($hasMedia)
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
@endif

class <?php echo $model; ?> extends Model @if ($hasMedia)implements HasMedia @endif
{
    @if ($hasMedia)use HasMediaTrait;@endif
    //
    @if ($fillable != '')
    protected $fillable = {!! $fillable !!};
    @endif
}
