@extends('layouts.admin')

@section('content')
<div class="page-header clearfix">
    <div class="title pull-left">
        <h1>{{$contentType->get('name')}}</h1>
        <h2>Edit</h2>    
    </div>
    <div class="actions pull-right">
    </div>
</div>

@include('partials.errors')
@include('partials.success')
<div class="row">
    <div class="col-md-2">
    @include('contentitem_menu')
    </div>
    <div class="col-md-10">
      <form method="post" action="{{action('ContentController@relations_belongstomany_store', ['type' => $contentType->type, 'id' => $item->id, 'belongsToMany' => $field_name])}}" class="form">
        {{csrf_field()}}
        <ul id="specslist">
          @foreach($items_per_tag as $tag)
          <li>
            <span class="specification">{{$tag['name']}}</span>
            <ul>
            @foreach($tag['data'] as $data_item_value => $data_item_label)
              <li class="spec-item">
                <label>
                  <input type="checkbox" name="relation[]" value="{{$data_item_value}}" @if (in_array($data_item_value, $values_selected))checked="checked"@endif />&nbsp;{{$data_item_label}} ({{$data_item_value}})
                </label>
              </li>
            @endforeach
            </ul>
          </li>
          @endforeach
        </ul>
        
        <button class="btn btn-primary" type="submit" name="save">save</button>
      </form>
    </div>
</div>
@endsection
