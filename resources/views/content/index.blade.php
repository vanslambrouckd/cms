@extends('layouts.admin')


@section('content')
<div id="demo">

@include('partials.success')
<div class="page-header clearfix">
    <div class="title pull-left">
        <h1>{{$contentType->get('name')}}</h1>
        <h2>Overview</h2>    
    </div>
    <div class="actions pull-right">
        @if ($has_searchable_fields)
        <a href="#" class="btn btn-default btn-filter" @click.prevent="search_active = !search_active" data-toggle="modal" data-target="#searchform"><i class="fa fa-filter" aria-hidden="true"></i>filters</a>
        @endif

        <a class="btn btn-default btn-add" href="{{route('content.create', ['contentType' => $contentType->type])}}"><i class="fa fa-plus" aria-hidden="true"></i>add item</a>
        <!-- <a class="btn btn-default"><i class="fa fa-pencil" aria-hidden="true"></i></a>
        <a class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a> -->
    </div>
</div>

    @if (count($searchfilters_active))
    <div class="row content-index-filters">
        <div class="col-md-12">       
            <ul class="filterlist">
                <li>
                    <a href="#">Active filters:</a>
                </li>
            @foreach($searchfilters_active as $filter)
                <li>

                    {{$filter->name}}: <a href="{{action('ContentController@search_remove_field', ['type' => $contentType->type, 'field' => $filter->field_name])}}">{{$filter->get_postback_value_formatted($filter->postback_value)}} <i class="fa fa-times" aria-hidden="true"></i></a>                    
            @endforeach
                </li>
            </ul>

            <a class="pull-right" href="{{action('ContentController@search_clear_all', ['type' => $contentType->type])}}">Clear all</a>
        </div>    
    </div>
    @endif

    <div class="row">
        <div class="col-md-12">
              @if ($has_pagination)
                {{ $items->appends($pagination_appends)->links() }}
                @endif

            <!-- search val: <input v-on:keyup="quicksearch" type="text" class="form-control" id="searchval" v-model="searchval" /> @{{ searchval}}
            <datagrid :rows="rows" :fields="fields">
            </datagrid> -->

              <!-- <datagrid :rows="rows">
              </datagrid> -->
                <table class="table table-striped table-bordered">
                    <thead>
                        {!! $contentType->render_index_head() !!}
                    </thead>
                    <tbody>
                        {!! $contentType->render_index_body($items) !!}
                    </tbody>    
                </table>  
        </div>
    </div>    
</div>

{!! $contentType->render_searchform($searchparams) !!}

@endsection    