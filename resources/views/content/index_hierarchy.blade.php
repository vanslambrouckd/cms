@extends('layouts.admin')


@section('content')

@include('partials.success')

<div class="page-header clearfix">
    <div class="title pull-left">
        <h1>{{$contentType->get('name')}}</h1>
        <h2>Overview</h2>    
    </div>
    <div class="actions pull-right">
        <a class="btn btn-default btn-add" href="{{route('content.create', ['contentType' => $contentType->type])}}"><i class="fa fa-plus" aria-hidden="true"></i>add item</a>

    </div>
</div>

<table class="table table-striped table-bordered">
    <thead>
        @foreach($fields as $item)
        <th>{{$item->name}}</th>
        @endforeach
        @foreach($images as $img)
        <th>{{$img->title}}</th>
        @endforeach
        <th class="actions">Action</th>
        <th class="direction">Move</th>
    </thead>
    <tbody>
    @foreach($items as $key => $item)        
    <tr>
        @foreach($fields as $index => $field)
        <td class="one wide" style="padding-left:<?php echo ($item->getLevel()*30)+10; ?>px;">
            <?php
            $prev = isset($items[$key-1])?$items[$key-1]->id:null;
            $next = isset($items[$key+1])?$items[$key+1]->id:null;
            ?>
            <!-- prev = <?php echo $prev; ?><hr />
            next = <?php echo $next; ?><hr /> -->
            <!-- @if ($field->field_name == 'title')
                @for ($i = 0; $i < $item->getLevel(); $i++)
                ---
                @endfor
            @endif -->
            {{$field->postback_value($item, $field->field_name, 1)}}
        </td>
        @endforeach
        @foreach($images as $img)
        <td>
            <?php
            $img_src = $item->getFirstMediaUrl($img->name);
            if ($img_src != null) {
                echo '<img src="'.$img_src.'" alt="" title="" style="max-width:100px;" />';
            }            
            ?>            
        </td>
        @endforeach
        <td class="actions">
            <a class="btn btn-default btn-edit" href="{{action('ContentController@edit', ['type' => $contentType->type, 'content' => $item[$pk]])}}"></a>
            <form method="post" action="{{action('ContentController@destroy', ['type' => $contentType->type, 'content' => $item[$pk]])}}">
                {{ method_field('DELETE') }}                
                {{csrf_field()}}        
                <button class="btn btn-danger btn-delete" type="submit" name="delete"></button>                        
            </form>
        </td>
        <td>
            @if ($prev != '')
            <form method="post" action="{{action('ContentController@indexSort', ['type' => $contentType->type, 'content' => $item[$pk], 'direction' => 'up'])}}" style="display:inline-block;">
                {{ method_field('POST') }}                
                {{csrf_field()}}     
                <button class="btn btn-default" type="submit"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>
            </form>
            @endif

            <form method="post" action="{{action('ContentController@indexSort', ['type' => $contentType->type, 'content' => $item[$pk], 'direction' => 'left'])}}" style="display:inline-block;">
                {{ method_field('POST') }}                
                {{csrf_field()}}     
                <button class="btn btn-default" type="submit"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
            </form>

             @if ($next != '')
            <form method="post" action="{{action('ContentController@indexSort', ['type' => $contentType->type, 'content' => $item[$pk], 'direction' => 'down'])}}" style="display:inline-block;">
                {{ method_field('POST') }}                
                {{csrf_field()}}     
                <button class="btn btn-default" type="submit"><i class="fa fa-arrow-down" aria-hidden="true"></i></button>
            </form>
            @endif

            

            <form method="post" action="{{action('ContentController@indexSort', ['type' => $contentType->type, 'content' => $item[$pk], 'direction' => 'right'])}}" style="display:inline-block;">
                {{ method_field('POST') }}                
                {{csrf_field()}}     
                <button class="btn btn-default" type="submit"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>    
</table>
@endsection    