<th>
@if ($field->is_sortable())
    <a href="{{action('ContentController@index', ['type' => $contentType->type, 'order_field' => $field->field_name, 'order' => $order])}}">{{$field->name}}</a>
@else
    {{$field->name}}
@endif
</th>