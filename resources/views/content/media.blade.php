@extends('layouts.admin')
@section('content')

<div class="page-header clearfix">
  <div class="title pull-left">
    <h1>{{$contentType->get('name')}}</h1>
    <h2>Media</h2>    
  </div>
  <div class="actions pull-right">
  </div>
</div>

@include('partials.errors')
@include('partials.success')
<div class="row">
  <div class="col-md-2">
    @include('contentitem_menu')
  </div>

    <div class="col-md-10">
      <app-dropzone-uploader  href="{{action('ContentController@mediaStore', ['type' => $contentType->type, 'id' => $item->id, 'collection_name' => $collection_name])}}" id="media-uploader"></app-dropzone-uploader>
      <app-dropzone-list href="{{action('ContentController@mediaIndex', ['type' => $contentType->type, 'id' => $item->id, 'collection_name' => $collection_name, 'response_type' => 'json'])}}" :headers="{{json_encode($current_media_headings)}}"></app-dropzone-list>
    </div>
  </div>

  @endsection


  @section('js-footer')
  @parent
  <script type="text/javascript">
  </script>
  @endsection