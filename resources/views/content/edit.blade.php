@extends('layouts.admin')


@section('content')
<div class="page-header clearfix">
    <div class="title pull-left">
        <h1>{{$contentType->get('name')}}</h1>
        <h2>Edit</h2>    
    </div>
    <div class="actions pull-right">
      <a class="btn btn-default btn-back" href="{{$url_overview}}">back to overview</a>
    </div>
</div>

@include('partials.errors')
@include('partials.success')

<div class="row">
    <div class="col-md-2">
    @include('contentitem_menu')
    </div>

    <div class="col-md-10">
        <form method="post" action="{{action('ContentController@update', ['type' => $contentType->type, 'id' => $item->id])}}" class="form">
        {{csrf_field()}}
        {{ method_field('PATCH') }}

        {!! $fields_html !!}
        <button class="btn btn-primary" type="submit" name="save">save</button>
        </form>
    </div>
</div>
@endsection