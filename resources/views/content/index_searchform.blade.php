<modal title="{{$contentType->get('name')}} zoeken" :allow_close=true id="searchform">
    <form method="post" action="{{action('ContentController@filter', ['type' => $contentType->type])}}">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    {{csrf_field()}}
                    <div class="row">   
                        <div class="col-md-12">
                        @foreach($fields as $field)
                            {!! $field->render('contentfields.'.$field->render_type_search) !!}
                        @endforeach
                        </div>                
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Sluiten</button>
            <button type="submit" class="btn btn-primary">Zoeken</button>        
        </div>
    </form>
</modal>