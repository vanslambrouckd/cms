@if ($is_collection)
<div class="form-group">
  <label for="">{{$field->name}}</label>
      <div class="checkbox">
        @foreach($field->data as $chk)
        <input type="radio" name="{{$field->field_name}}" value="{{$chk->value}}" @if ($field->postback_value == $chk->value) checked="checked" @endif />{{$chk->label}}
        @endforeach  
    </div>
</div>
@else
<div class="radio">
  <label>
    <input type="radio" name="{{$field->field_name}}" value="1" {{$str_readonly}} @if ($field->postback_value == 1) checked="checked" @endif />{{$field->name}}
  </label>
  
</div>
@endif