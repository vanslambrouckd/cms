<div class="form-group">
  <label for="">{{$field->name}}</label>
  <select class="select form-control" name="{{$field->field_name}}" {{$str_readonly}}>
    <option value="" @if ($field->postback_value == "")selected="selected"@endif></option>
    @foreach($field->data as $item)
    <option value="{{$item->value}}" @if ($item->value == $field->postback_value)selected="selected"@endif>{{$item->label}}</option>
    @endforeach
    </select>
</div>