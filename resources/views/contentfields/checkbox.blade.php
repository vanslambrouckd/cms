@if ($field->field_type == 'boolean')
<div class="checkbox">
  <label>
    <input type="hidden" name="{{$field->field_name}}" value="0" />
    <input type="checkbox" name="{{$field->field_name}}" value="1" {{$str_readonly}} @if ($field->postback_value == 1) checked="checked" @endif />{{$field->name}}
  </label>
</div>
@else
<?php
if (!is_array($field->postback_value)){
    $field->postback_value = []; //todo:fix
}
?>
<div class="form-group">
  <label for="">{{$field->name}}</label>
    @foreach($field->data as $chk)
    <div class="checkbox">
      <label>
          <input type="checkbox" class="checkbox" name="{{$field->field_name}}[]" value="{{$chk->value}}" @if (in_array($chk->value, $field->postback_value)) checked="checked" @endif />{{$chk->label}}
     </label>
    </div>
    @endforeach
</div>
@endif