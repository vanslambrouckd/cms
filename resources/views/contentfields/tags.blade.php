<div class="form-group">
  <label  for="">{{$field->name}}</label>
  <select name="{{$field->field_name}}[]" {{$str_readonly}} class="tags select form-control" multiple="multiple">
    <option value=""></option>
    {{-- field data = all tags --}}

    @foreach($field->data as $item) 
    <option value="{{$item->name}}" @if (in_array($item->id, $selected_tag_ids))selected="selected"@endif>{{$item->name}} ({{$item->id}})</option>
    @endforeach
    </select>      
</div>