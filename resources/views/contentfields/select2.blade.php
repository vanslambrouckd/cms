<div class="form-group">
  <label for="">{{$field->name}}</label>
<!--   <select name="{{$field->field_name}}" {{$str_readonly}} class="form-control select select2"> -->
  <select name="{{$field->field_name}}" {{$str_readonly}} class="form-control select select2">
        <option value=""></option>

        @if (!empty($field->data))
        @foreach($field->data as $item)
        <option value="{{$item->value}}" @if ($item->value == $field->postback_value)selected="selected"@endif>{{$item->label}}</option>
        @endforeach
        @endif
    </select>
</div>