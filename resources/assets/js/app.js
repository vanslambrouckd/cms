
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');
 
 window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 Vue.component('example-component', require('./components/ExampleComponent.vue'));
 Vue.component('app-dropzone-list', require('./components/dropzoneList.vue'));
 Vue.component('app-dropzone-row', require('./components/dropzoneRow.vue'));
 Vue.component('app-dropzone-uploader', require('./components/dropzoneUploader.vue'));

import Dropzone from 'dropzone';

Dropzone.options.mediaDropzone = {
  parallelUploads: 1,
  autoDiscover: false,
  // init: function () {
  //   this.on("complete", function (file) {
  //     //location.reload();
  //   });
  // }
};

//import select2 from 'select2';
import select2 from 'select2/dist/js/select2.full.js';
import jqueryui from 'jquery-ui';

//window.select2 = select2;

import App from './App.vue';
//import datagrid from './datagrid.vue'
//Vue.component('datagrid', require('./components/datagrid.vue'));

// var app = new Vue({
//   el: '#app',
//   data: {
//     message: 'Hello Vue!'
//   }
// });

// new Vue({
//   el: '#app',
//   render: h => h(App)
// });

Vue.component('modal', require('./components/modal.vue'));

//axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
//console.log(document.querySelector('meta[name="csrf-token"]').getAttribute('content'));
// let token = $('meta[name="csrf-token"]').attr('content');
// console.log('token='+token);

// var drop = new Dropzone('#test', {
//   url: $('#test').attr('data-url-post'),
//   headers: {
//       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//   }
// });

// let media = new Vue({
//   el: '#test',
//   data: {
//     media: [
//       1,2,3
//     ],
//     dropzoneUrlList: '',
//     dropzoneUrl: '',
//     token: '',
//     drop: null,
//   },
//   created() {
//     this.token = $('meta[name="csrf-token"]').attr('content');
//     this.dropzoneUrl = $('#test').attr('data-url-post');
//     console.log('url='+this.dropzoneUrl);
//     this.dropzoneUrlList = $('#test').attr('data-url-list');
//     console.log(this.dropzoneUrlList);
//     this.drop = new Dropzone('#test', {
//       url: 'http://www.test.com',
//       headers: {
//           'X-CSRF-TOKEN': this.token
//       }
//     });
//     this.drop.on('complete', (file) => {
//       console.log('jaaa');
//       this.getMedia();
//     });

//     this.getMedia();

//   },
//   methods: {
//     getMedia() {
//       axios.get(this.dropzoneUrlList)
//       .then((response) => {
//         this.media = response.data;
//       });
//     },
//     uploadMedia() {

//     }
//   }
// });

export const eventBus = new Vue();

$.ajaxSetup({
    headers:
    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});


// new Vue({
//   el: '#app',
//   render: h => h(App)
// });

var demo = new Vue({
  el: '#app',
  data: {   
  },
  methods: {
    // editMediaMeta: function() {
    //   console.log('editMediaMeta');
    //   axios.get('http://cms.dev/admin/content/products/1/media_meta_edit/images/1')
    //   .then(function (response) {
    //   console.log(response);
    //   var $obj = $('#modal-media-meta');
    //   console.log(response.data);
    //   $('.modal-body').html(response.data);
    //   //$obj.find(".modal-body").html("testfdsqf");
    //   $obj.modal('toggle');
    // })
    // .catch(function (error) {
    //   console.log(error);
    // });
    //   //$('#modal-media-meta').modal('toggle');
    // }
  },
  mounted: function() {
    $('.select2').select2({
      theme: "bootstrap"
    });

    $('.tags').select2({
      theme: "bootstrap",
      tags: true,
      createTag: function (params) {
        //console.log(params);
        return {
          id: params.term,
          text: params.term,
          newOption: true
        }
      },
      templateResult: function (data) {
        var $result = $("<span></span>");

        $result.text(data.text);

        if (data.newOption) {
          $result.append(" <em>(new)</em>");
        }

        return $result;
      }
    });
  }
});