let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   //.sourceMaps();

mix.styles([
     'node_modules/select2-bootstrap-theme/dist/select2-bootstrap.css',
], 'public/css/custom.css');

// mix.styles([
//     // 'resources/assets/css/normalize.css',
//     // 'resources/assets/css/main.css',
//     // 'resources/assets/css/form.css',
//     //'resources/assets/font-awesome/css/font-awesome.css',
// ], 'public/css/app.css');

// mix.combine([
//     'resources/assets/normalize.css',
//     'resources/assets/main.css',
// ], 'public/css/boilerplate.css');

// mix.scripts([
//     'resources/assets/js/modernizr-3.5.0.min.js',
// ], 'public/js/modernizr-3.5.0.min.js');


// mix.scripts([
//     'resources/assets/js/bootstrap.js',
//     //'resources/assets/js/axios.min.js'
//     ],
//     'public/js/axios.min.js'
// );