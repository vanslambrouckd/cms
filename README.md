#WIP CMS  

1. setup using laravel valet  (cms.dev)  
2. import cms.sql (php artisan migrate:refresh --seed niet meer nodig dan)  
3. npm run dev  
4. surf naar cms.dev/admin  

##TODO:
media sortorder + info kunnen toevoegen (bvb title, description)  
MENU ITEMS SORTABLE + HIERARCHY (http://ilikenwf.github.io/example.html)  
product categorie link (one to many, many to many, ...);  

laravel package maken voor cms gedeelte (https://laravel.com/docs/5.5/packages)  
block package maken (zie asgardcms plugin), om kleine tekstjes op verschillende paginas te tonen  

bij overview van content, filters zoals in het admingedeelte van magento store + overview pager  

overview tabel => cellen kunnen overschrijven  
0 product media images => sortable maken via sortablejs (http://rubaxa.github.io/Sortable/
https://jsfiddle.net/pe7jy5w4/2/)
0. product media images => doordoen modals
1. product add => bij postback wordt tag keuze niet onthouden + numeric , ook bruikbaar maken
1. product images edit => via popup
2. forms + builder module  
4. searchfilter tags => tussen and / or kunnen kiezen voor meerdere tags  
5. filter hasmany checkbox vuejs  
6. render_type_search => ook select2 mogelijk maken (select2 werkt nu niet door vuejs combinatie)  
7. product tags / merk => select2 werkt niet meer door vue js!  
8. doordoen forms => form fields: via popup kunnen toevoegen  
9. config files via cms zelf beheerbaar maken  
10. todos oplossen  
11. menus maken via https://docs.spatie.be/menu/v2/controlling-the-html-output/item-attributes  
12. admin overview sortable na terugkeren van edit  
13. form builder (form fields sortorder)  
14. meertaligheid  
15. tests schrijven  

##INFO  
status relationships:  
hasOne OK / belongsTo NOK  
hasMany OK (maar editable maken, zie brand edit / country edit)  

data_type => json / query / function  

in configs moet data_type vervangen worden door field_type  

alles heeft:  
field_type (hasMany, belongsToMany, hierarchy, json, custom, boolean, query, tags)  
render_type (textarea, textfield, select, select2, checkbox, radio)  
 
relation (hasOne, hasMany)  