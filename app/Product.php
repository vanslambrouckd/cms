<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\Tags\HasTags;

class Product extends Model implements HasMedia
{
    use HasTags;
    use HasMediaTrait;

    protected $fillable = ['title', 'price', 'price_promo', 'description', 'brand_id', 'stock', 'secondhand'];
    //
    public function categories() 
    {
        return $this->belongsToMany(Category::class);
    }

    public function specifications() 
    {
        return $this->belongsToMany(Specifications::class)->withPivot('value');
    }

    public function brand() {
        return $this->belongsTo(Brand::class);        
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }
}
