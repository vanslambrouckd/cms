<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use \App\Http\Requests\ContentRequest;
use Route;

class CmsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        //dd(Route::current());
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Http\Requests\ContentRequest', function($app, $parameters) {
            $type = $parameters['type'];
            $classname_formrequest = config('cms.contenttypes.'.$type.'.formrequest');
            $classname = 'App\\Http\Requests\\'.$classname_formrequest;
            if (class_exists($classname)) {
                $req = new $classname;
            } else {
                $id = $parameters['id'];
                $req =  new ContentRequest();
                $req->content_type = $type;
                $req->id = $id;
            }
            return $req;
       });
    }
}
