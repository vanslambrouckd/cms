<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        View::composer(
            'content.*', 'App\Http\ViewComposers\ContentComposer'
        );

        View::composer(
            [
                'content.create', 
                'content.edit', 
                'content.media', 
                'content.relations_belongstomany',
                'content.relations_belongstomany_by_tag',
                'content.relations_hasmany',
            ], 
            'App\Http\ViewComposers\ContentComposer@compose_menu'
        );

        // Using Closure based composers...
        // View::composer('dashboard', function ($view) {
        //     //
        // });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}