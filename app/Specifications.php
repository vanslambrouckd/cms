<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Tags\HasTags;

class Specifications extends Model
{
    //
    use HasTags;
    protected $fillable = ['specification'];

    public function products() 
    {
        return $this->belongsToMany(Product::class);
    }
}
