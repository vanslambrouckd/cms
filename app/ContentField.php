<?php
namespace App;
use Log;
use View;
use File;
use Exception;
use Config;
use DB;

class ContentField {
    public $name;
    public $field_name;
    public $field_type;
    public $relation;
    public $data;
    public $rules; //validation rules
    public $postback_value;
    public $searchable;
    public $render_type_search;

    public function __construct($field) 
    {
        $this->name = $field['name'];
        $this->field_name = $field['field_name'] ?? null;        
        $this->field_type = $field['field_type'] ?? '';
        $this->render_type = $field['render_type'] ?? 'textfield';
        $this->render_type_search = $field['render_type_search'] ??$this->render_type;
        $this->relation = $field['relation'] ?? null;

        $this->template = $field['template'] ?? null; //admin veld template overschrijven
        
        //voor belongsToMany
        $this->field_type = $field['field_type'] ?? null;
        $this->data_value = $field['data_value'] ?? null;

        $this->data_class = $field['data_class'] ?? null;
        $this->data_function = $field['data_function'] ?? null;
        $this->data_value = $field['data_value'] ?? 'id';
        $this->data_label = $field['data_label'] ?? 'id';

        //voor contentrelation
        $this->related_class = $field['related_class'] ?? '';
        $this->related_field = $field['related_field'] ?? '';
        $this->searchable = $field['searchable'] ?? 0;
        if ($this->searchable != 1) {
            $this->searchable = 0;
        }

        $this->rules = $field['rules'] ?? '';
        $this->heading = $field['heading'] ?? 0;

        if ($this->heading != 1) {
            $this->heading = 0;
        }

        $this->edit = $field['edit'] ?? 1;
        if (!in_array($this->edit, [1,2])) {
            //1 = editable, 2 = read only visable
            $this->edit = 0;
        }
        $this->query = $field['query'] ?? null;

        if (!empty($this->field_type)) {
            switch($this->field_type) {
                case 'json';
                $ret = $this->get_data_array($this->data_value);
                $this->set_data($ret);
                break;

                case 'query':
                $this->set_data($this->get_field_query());
                break;
            }
        } else {
            $data = $this->hasRelation()?$this->field_data():[];            
            $this->set_data($data);
            // if (in_array($this->field_name, ['reviews']))
            // {
            //     dd($this);
            // }
        }        
    }

    public function get_data_array($content) 
    {
        /*
        1;david
        2;naam2

        => returns object met key val
        */
        $ret = [];
        $arr = explode("\n", $content);
        if (!empty($arr)) {
            foreach($arr as $item) {
                $tmp = explode(';', $item);
                if (count($tmp) == 2)
                {
                    $ret[] = (object)['value' => trim($tmp[0]), 'label' => trim($tmp[1])];
                }
            }    
        }
        
        return (object)$ret;
    }

    public function hasRelation():bool
    {
        return ($this->relation != null);
    }

    public function is_readonly():bool
    {
        return !($this->edit ==1);
    }

    public function get_has_many_data($model_instance)
    {
        $data_class = $this->data_class;
        $data_function = $this->data_function;
        $tmp = $model_instance->{$data_function};

        $ret = array();
        if (count($tmp))
        {
            foreach($tmp as $item)
            {
                $row = (object)[
                    'value' => $item[$this->data_value],
                    'label' => $item[$this->data_label],
                ];
                $ret[] = $row;
            }
        }
        $ret = collect($ret);
        return $ret;
    }

    private function get_field_query() {
        $data = [];
        $field_type = isset($this->field_type)?$this->field_type:'';
        if ($field_type == 'query')
        {
            if (!empty($this->data_value)) {
                $tmp = DB::select(DB::raw($this->data_value));
                foreach($tmp as $item) {
                    $data[$item->value] = (object)['value' => $item->value, 'label' => $item->label];
                }
            }
        }
        
        return $data;
    }

    /*
    returns relation data waaruit je kan kiezen
    */
    private function field_data()
    {
        $data = [];
        if ($this->hasRelation()) {
            switch($this->relation) {
                case 'hasOne':
                $data = $this->get_field_query();
                break;

                case 'hasMany':
                case 'belongsToMany':
                    //returns Model collection
                $data_class = $this->data_class;
                $data_function = $this->data_function;
                $tmp = call_user_func_array(array($data_class, $data_function), array());
                foreach($tmp as $item) {
                    $data[$item->{$this->data_value}] = (object)['value' => $item->{$this->data_value}, 'label' => $item->{$this->data_label}];
                }
                break;
                // case 'hasMany':
                // break;
            }
        }
        return $data;
    }

    public function set_postback_value($row, $postback_type = 0) 
    {
        //$this->postback_value = $this->postback_value($row, $this->field_name);
        $this->postback_value = old($this->field_name, $this->postback_value($row, $this->field_name));
    }

    // public function get_postback_value()
    // {
    //     return $this->postback_value;
    // }

    public function postback_value($row, string $field_name, $postback_type = 0)
    {
        /*
        postback value:
        0 = de numerieke waarde
        1 = de label waarde
        bij select moet je de value returnen (1 ipv belgie bvb)
        bij textfield de waarde zelf
        */        
        //row = Illuminate\Database\Eloquent\Model
        $ret = '';
        if (!empty($row)) {
            //$attributes = $row->getAttributes();
            $ret  = $row->{$field_name};    

            if ( ($this->field_type == 'tags') && ($postback_type == 1) ) {
                $tag_names = $row->tags->pluck('name')->toArray();
                $tag_names = implode(', ', $tag_names);
                $ret = $tag_names;
            }

            if ( ($this->field_type == 'boolean') && ($postback_type == 1) ) {
                $ret = ($ret == 1)?'True':'False';
            }

            if ($this->hasRelation() && $postback_type == 1) {
                if (!empty($this->data)) {
                    $ret = $this->data[$ret]->label ?? $ret;
                }
            }
        }

        if ($ret instanceof \Illuminate\Database\Eloquent\Collection){
            if ($postback_type == 0)
            {
                $ret = $row->{$field_name}->pluck($this->data_value)->toArray();
                // if ($field_name == 'reviews') {
                //     dd($row->reviews);
                // }
            }
        }

        return $ret;
    }

    public function get_postback_value_formatted($value_unformatted) 
    {
        $ret = '';
        if ($this->field_type == 'boolean') {
            $ret = ($value_unformatted == 1)?'True':'False';
        } elseif ($this->hasRelation()) {
            if (!empty($this->data)) {
                if (is_array($value_unformatted)) {
                    foreach($value_unformatted as $val) {
                        $item = isset($this->data[$val])?$this->data[$val]->label:'';
                        if (strlen(trim($item)) > 0) {
                            $items[] = $item;
                        }
                    }
                    if (!empty($items)) {
                        $ret = implode(', ', $items);
                    }
                } else {
                    $ret = $this->data[$value_unformatted]->label ?? $ret;                        
                }
            }
        } elseif ($this->field_type == 'tags') {
            $ret = implode(', ', $value_unformatted);
        } else {
            $ret = $value_unformatted;
        }
        return $ret;
    }

    public function get_render_type()
    {
        return $this->render_type;
    }

    public function render($template_name = '')
    {
        switch($this->get_render_type()) {
            case 'hidden':
            return $this->render_hidden();
            break;

            case 'hierarchy':
            return $this->render_select($template_name);
            break;
            case 'password':
            return $this->render_password();                
            break;

            case 'textfield':                
            return $this->render_textfield($template_name);                
            break;

            case 'email':                
            return $this->render_email($template_name);                
            break;

            case 'textarea':
            return $this->render_textarea($template_name);                
            break;

            case 'select':
            return $this->render_select($template_name);                
            break;

            case 'select2':
            return $this->render_select2($template_name);                
            break;

            case 'tags':
            return $this->render_tags();                
            break;

            case 'checkbox':
            return $this->render_checkbox($template_name);
            break;

            case 'radio':
            return $this->render_radio();
            break;
        }
    }

    public function is_collection() {
        return ($this->field_type == 'json' || ($this->relation == 'hasMany'));
    }

    public function set_data($data) {
        $this->data = $data;
        // if ($this->field_name == 'reviews') {
        //     Log::info('contentfield.set_data for field '.$this->field_name);    
        //     dd($this->data);
        // }
    }

    public function render_checkbox($template_name) {
        $template_name = !empty($template_name)?$template_name:'contentfields.checkbox';
//dd($this->data);
        Log::info('render_checkbox field_name='.$this->field_name);
        if ($this->field_name == 'reviews') {
            //dd($this);
        }
        $viewdata = [
            'field' => $this,
            'str_readonly' => $this->is_readonly()?' readonly="readonly" ':'',
        ];        
        $view = View::make($template_name, $viewdata);        
        $data = $view->render();
        return $data;
    }

    public function render_select2($template_name) {
        $template_name = !empty($template_name)?$template_name:'contentfields.select2';

        $viewdata = [
            'field' => $this,
            'str_readonly' => $this->is_readonly()?' readonly="readonly" ':'',
        ];
        //dd($this->data);

        $view = View::make($template_name, $viewdata);        
        $data = $view->render();
        return $data;
    }

    public function render_radio() {
        $viewdata = [
            'field' => $this,
            'str_readonly' => $this->is_readonly()?' readonly="readonly" ':'',
            'is_collection' => ($this->field_type == 'json' || ($this->relation == 'hasMany'))
        ];
        $view = View::make('contentfields.radio', $viewdata);        
        $data = $view->render();
        return $data;
    }

    public function render_has_many() {
        $viewdata = [
            'field' => $this,
        ];
        $view = View::make('contentfields.has_many', $viewdata);        
        $data = $view->render();
        return $data;
    }

    public function render_tags() {
        //dd($this->postback_value);

        // if (!$this->postback_value instanceof Illuminate\Database\Eloquent\Collection) {
        //     $this->postback_value = collect($this->postback_value);
        // }

        //dd($this->postback_value->pluck('id'));
        // if (!is_array($this->postback_value)) {
        //     $this->postback_value = [$this->postback_value];
        //     dd($this->postback_value);
        // }
        $selected_tag_ids = array_values($this->postback_value);
        $viewdata = [
            'field' => $this,
            'str_readonly' => $this->is_readonly()?' readonly="readonly" ':'',
            //'selected_tag_ids' => $this->postback_value->pluck('id')->toArray(),
            'selected_tag_ids' => $selected_tag_ids,
        ];
        $view = View::make('contentfields.tags', $viewdata);        
        $data = $view->render();
        return $data;
    }

    public function render_select($template_name) {
        $template_name = !empty($template_name)?$template_name:'contentfields.select';

        $viewdata = [
            'field' => $this,
            'str_readonly' => $this->is_readonly()?' readonly="readonly" ':'',
        ];
        //dd($this->data);
        $view = View::make($template_name, $viewdata);                
        $data = $view->render();
        return $data;
    }

    public function render_textarea($template_name) {
        $template_name = !empty($template_name)?$template_name:'contentfields.textarea';


        $viewdata = [
            'field' => $this,
            'str_readonly' => $this->is_readonly()?' readonly="readonly" ':'',
        ];

//         if (!empty($template_name)) {
//     if ($this->field_name == 'receiver_emails') {
//     dd($template_name);

//     }
// }
        $view = View::make($template_name, $viewdata);        
        
        $data = $view->render();
        return $data;
    }

    public function render_textfield($template_name)
    {
        $template_name = !empty($template_name)?$template_name:'contentfields.textfield';

        $viewdata = [
            'field' => $this,
            'str_readonly' => $this->is_readonly()?' readonly="readonly" ':'',
        ];
        $view = View::make($template_name, $viewdata);        

        $data = $view->render();
        return $data;
    }

    public function render_email($template_name)
    {
        $template_name = !empty($template_name)?$template_name:'contentfields.email';

        $viewdata = [
            'field' => $this,
            'str_readonly' => $this->is_readonly()?' readonly="readonly" ':'',
        ];
        $view = View::make($template_name, $viewdata);        

        $data = $view->render();
        return $data;
    }

    public function render_hidden()
    {
        $viewdata = [
            'field' => $this,
        ];

        $view = View::make('contentfields.hidden', $viewdata);                
        $data = $view->render();
        return $data;
    }

    public function render_password()
    {
        $viewdata = [
            'field' => $this,
            'str_readonly' => $this->is_readonly()?' readonly="readonly" ':'',
        ];
        $view = View::make('contentfields.password', $viewdata);        
        $data = $view->render();
        return $data;
    }

    public function is_sortable(): bool
    {
        //return true if sortable in admin overview
        return (in_array($this->field_type, [
            'textfield',
            'textarea',
            'boolean',
        ]));
    }

    public function is_searchable()
    {        
        return ($this->searchable == 1);
    }

    public function is_editable($pk)
    {
        if ($this->field_name == $pk) {
            return false;
        }

        if ($this->hasRelation()) {
            if (in_array($this->relation, ['belongsToMany'])) {
                return false;
            }
        }

        if ($this->edit == 0) {
            return false;
        }
        return true;
    }
}