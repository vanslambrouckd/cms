<?php
namespace App;
use View;
use File;
use Exception;
use Config;
use DB;

class ContentType {
    private $fields;

    private $data;
    protected $fillable;
    private $pk;
    public $type;
    private $model;
    private $configKey;

    public function getPk(): string
    {
        return $this->pk[0];
    }

    public function __construct($type) {
        $this->type = $type;
        $this->configKey = 'cms.contenttypes.'.$type;      

        //read config file
        $data = config($this->configKey);
        // dd($data);
        // dd($this->configKey);
        if (empty($data)) {
            throw new Exception('no valid content type config found: '.$this->configKey);
        }
        $fields = $this->get('fields');

        if (!empty($fields)) {
            foreach($fields as $key => $field) {
                $fld = new ContentField($field);                
                $this->fields[] = $fld;                            
            }    
            //dd($this->fields);
        }

        $b_create_model = $this->get('create_model', true);

        $this->fillable = collect($this->fields)->pluck('field_name')->toArray();
        $this->pk = $this->get('pk');
        if (!is_array($this->pk)) $this->pk = (array)$this->pk;
        $this->model = $this->get('model');
        if ($b_create_model) {
            if (!$this->modelClassExists($type)) {
                $this->createFile();
            }    
        }
    }

    public function current_media($collection_name): array
    {
        $items = collect($this->get('media'))->filter(function ($value, $key) use ($collection_name) {
            return ($value['name'] == $collection_name);
        });
        return $items->toArray();
    }

    public function current_mediacollection_properties($collection_name): array
    {
        $ret = [];
        $item = collect($this->current_media($collection_name))->first();
        if (!empty($item['properties'])) {
            $ret = $item['properties'];
        }
        return $ret;
    }

    public function current_mediacollection_headings($collection_name): array
    {
        $ret = [];
        $items = collect($this->current_media($collection_name));
        foreach($items as $item) 
        {
            if (!empty($item['properties'])) {
                foreach($item['properties'] as $property_key => $property) {
                    if (isset($property['heading']) && $property['heading'] == 1) {
                        $ret[] = $property_key;
                    }
                }
            }
        }
        return $ret;
    }

    public function media():array
    {
        $media = [];
        $items = $this->get('media');
        if (!empty($items)) {
            foreach($items as $item) {
                $media[] = new ContentMedia($item);
            }    
        }
        return $media;
    }

    public function fields():array
    {
        return $this->fields;
    }

    public function fields_heading($arr_exclude_field_names): array
    {
        $headings = collect($this->fields())->filter(function ($value, $key) use ($arr_exclude_field_names) {
            return ($value->heading == 1) && (!in_array($value->field_name, $arr_exclude_field_names));
        });
        return $headings->toArray();
    }

    public function images_heading(): array
    {
        $headings = collect($this->media())->filter(function ($value, $key) {
            return ($value->heading == 1);
        });        
        return $headings->toArray();
    }

    public function fields_tags(): array
    {
        $fields = collect($this->fields())->filter(function ($value, $key) {
            if ($value->field_type == 'tags') {
                return true;
            }
            return false;
        });
        return $fields->toArray();   
    }

    public function fields_edit(): array
    {
        $fields = collect($this->fields())->filter(function ($value, $key) {
            return $value->is_editable($this->getPk());           
        });
        return $fields->toArray();   
    }

    public function fields_searchable(): array
    {
        $fields = collect($this->fields())->filter(function ($value, $key) {
            return $value->is_searchable();           
        });
        return $fields->toArray();   
    }


    public function get_validation_rules(): array
    {
        $rules = [];
        $fields = $this->fields_edit();
        foreach($fields as $field) {
            $rules[$field->field_name] = isset($field->rules)?$field->rules:'';
        }
        return $rules;
    }

    public function getModelName():string
    {
        $modelName = $this->model;
        $namespace = $this->get('model_namespace', "\App\\");
        $modelName = $namespace.$modelName;
        //$modelName = "\App\\".$modelName;
        return $modelName;
    }

    private function getFillable():string
    {
        $fillable = '';
        if (!empty($this->fillable)) {
            $fillable = json_encode($this->fillable);
        }
        return $fillable;
    }

    private function modelClassExists($model): bool
    {
        $filename = app_path().'/'.$model.'.php';   
        return File::exists($filename);
    }

    public function createFile() 
    {
        $filename = app_path().'/'.$this->model.'.php';
        if (!File::exists($filename)) {
            $viewdata = [
                'model' => $this->model,
                'contentType' => $this,
                'fillable' => $this->getFillable(),
                'hasMedia' => $this->hasMedia()
            ];

            $view = View::make('content.model_template', $viewdata);

            $data = $view->render();
            $data = str_replace('&lt;', '<', $data);
            File::put($filename, $data);
        }
    }

    public function get($key, $default_value = null) 
    {
        //echo $this->configKey.'<br />';
        $data = config($this->configKey);
        // if ($this->configKey == 'cms.contenttypes.users') {
        //     // echo 'key='.$key;
        //     // echo '<br />';
        //     if ($key == 'paginate') {
        //         echo $key;
        //         dd(array_get($data, $key));
        //     }
        // }
        $ret = array_get($data, $key);
        $ret = $ret??$default_value;
        return $ret;
    }

    public function hasMedia(): bool
    {
        $media = $this->media();
        return count($media);
    }

    public function get_belongs_to_many() {
        $fields = collect($this->fields())->filter(function ($value, $key) {
            if ($value->hasRelation()) {
                if ($value->relation == 'belongsToMany') {
                    return true;
                }
            }
            return false;
        });
        return $fields->toArray();          
    }

    public function get_has_many() {
        $fields = collect($this->fields())->filter(function ($value, $key) {
            if ($value->hasRelation()) {
                if ($value->relation == 'hasMany') {
                    return true;
                }
            }
            return false;
        });
        return $fields->toArray();          
    }

    public function get_content_field_by_fieldname($field_name)
    {
        foreach($this->fields as $field) 
        {
            if ($field->field_name == $field_name) {
                return $field;
            }
        }
        return null;
    }

    public function is_hierarchy():bool
    {
        $field = $this->get_hierarchy_field();
        return ($field != null);
    }

    public function get_hierarchy_field()
    {
        $fields = collect($this->fields())->filter(function ($value, $key) {
            if ($value->field_type == 'hierarchy') {
                return true;
            }
        });
        return $fields->first();
    }

    public function has_pagination()
    {
        if (!$this->is_hierarchy()) {
            $paginate = $this->get('paginate', null);
            return !empty($paginate);
        }
        return false;
    }

    public function get_tabs()
    {
        $relations = $this->get('tabs', []);
        return $relations;
    }

    public function get_relation_field_by_fieldname($field_name): ContentField
    {
        $fields = collect($this->get('tabs'))->filter(function($value, $key) use ($field_name) {
            $value = (object)$value;
            if (isset($value->field_name) && ($value->field_name == $field_name) ) {
                return true;
            }
        });
        $fld = $fields->first();
        if (!empty($fld)) {
            return new ContentField($fld);
        }
        // dd($fld);
        // return $fields->first();
    }

    public function group_data_by_tag($relation, $field_name, $model_instance) {

        $specs_per_tag = [];

        $data = $relation->data($model_instance);
        $values = array_keys($data); //values = tbl spec ids

        $tags = \Spatie\Tags\Tag::withType($field_name)->get();
        foreach($tags as $tag) {
            $specs_per_tag[$tag->id] = [
                'name' => $tag->name
            ];
        }

        $data_class = $relation->data_class;
        $data_function = $relation->data_function;
        $data_value = $relation->data_value;
        $data_label = $relation->data_label;
        $specs = call_user_func_array(array($data_class, $data_function), array());
        foreach($specs as $spec) {
            foreach($spec->tags as $t) {
                $specs_per_tag[$t->id]['data'][$spec->{$data_value}] = $spec->{$data_label};
            }
        }
        $specs_per_tag = $specs_per_tag;
        return $specs_per_tag;
    }

    public function set_postback_value(&$field, $model_instance)
    {
        if ($field->is_editable($this->getPk())) {
            if ($field->relation == 'hasMany') {
                $field->has_many_data = $field->get_has_many_data($model_instance);
            }

            //todo: refactor: item tags ophalen indien field_type = tags
            if ($field->field_type == 'tags') {
                $field->postback_value = $model_instance->tags;
                $field->set_data(\Spatie\Tags\Tag::withType($this->type)->get()); 
            }

            if ($field->field_type == 'hierarchy') {
                //call_user_func_array(function, param_arr)
                $nested_list = forward_static_call_array(array('App\\'.$this->model, 'getNestedList'), array($field->data_label, $field->data_value, '-&nbsp;&nbsp;'));
                $nl = [];
                if (!empty($nested_list)) {
                    foreach($nested_list as $value => $label) {
                        $nl[] = (object)[
                            'value' => $value,
                            'label' => $label,
                        ];
                    }  

                }
                $field->set_data($nl);
            }    
        }

        $field->set_postback_value($model_instance);
    }

    public function set_postback_values($model_instance)
    {
        if (!empty($this->fields))
        {
            foreach($this->fields as $key => $field) {
                $this->set_postback_value($field, $model_instance);
                $this->fields[$key] = $field;                
            }
        }
    }

    public function get_model_folder() {
        return strtolower($this->get('model'));
    }

    public function render_index_head() {
        $request = \Request::instance();
        //echo $request->get('order');
        $order = $request->get('order', $this->get('default_sortorder', 'desc'));

        $data = '';
        foreach($this->fields_heading([]) as $field)
        {
            $template = 'content.index_th';

            $custom_template = 'cmsextra.'.$this->get_model_folder().'.index_th_'.$field->field_name;
            if (View::exists($custom_template)){
                $template = $custom_template;
            }    

            $thdata = [
                'field' => $field,
                'contentType' => $this,
                'order' => ($order == 'asc')?'desc':'asc'
            ];
            $view = View::make($template, $thdata);        
            $data .= $view->render();            
        }

        //todo: opsplitsen
        foreach($this->images_heading() as $img)
        {
            $data .= sprintf('<th>%s</th>', $img->title);
        }

        $data .= '<th class="actions">action</th>';

        return $data;
    }

    public function render_index_body($items) {
        $data = '';
        foreach($items as $item) {
            $data .= $this->render_index_body_row($item);
        }
        return $data;
    }

    public function render_index_body_row($item) {
        $data = '';
        $data .= '<tr>';

        foreach($this->fields_heading([]) as $field) 
        {
            $val = $field->postback_value($item, $field->field_name, 1);
            $data .= sprintf('<td>%s</td>', $val);
        }          

        foreach($this->images_heading() as $img)
        {
            $val = '';
            $img_src = $item->getFirstMediaUrl($img->name);
            if ($img_src != null) {
                $val = '<img src="'.$img_src.'" alt="" title="" style="max-width:100px;" />';
            }            
            $data .= sprintf('<td>%s</td>', $val);
        }

        //actions
        $actionsdata = [
            'contentType' => $this,
            'action_edit' => action('ContentController@edit', ['type' => $this->type, 'content' => $item[$this->getPk()]]),
            'action_delete' => action('ContentController@destroy', ['type' => $this->type, 'content' => $item[$this->getPk()]]),
            'item' => $item,
        ];
        $template = 'content.index_actions';

        $custom_template = 'cmsextra.'.$this->get_model_folder().'.index_actions';
        if (View::exists($custom_template)){
            $template = $custom_template;
        }    

        $view = View::make($template, $actionsdata);        
        $data .= $view->render();

        $data .= '</tr>';

        return $data;  
    }

    public function render_fields() {
        $data = '';
        foreach($this->fields() as $field)
        {
            if ($field->is_editable($this->getPk())) {
                $template = '';
                $custom_template = 'cmsextra.'.$this->get_model_folder().'.'.$field->field_name;
                if (View::exists($custom_template)){
                    $template = $custom_template;
                }    
                $data .= $field->render($template);
            }
        }
        return $data;
    }

    public function get_active_searchfilters($searchparams) 
    {
        $fields = $this->fields_searchable();    
        
        $fields = collect($this->fields_searchable())->filter(function ($field, $key) use ($searchparams) {
            if (isset($searchparams[$field->field_name])) {
                $field->postback_value = $searchparams[$field->field_name]??'';
                return $field;
            }
        });
        //dd($fields);
        return $fields;
    }

    public function render_searchform($searchparams)
    {
        $html = '';
        $fields = $this->fields_searchable();    
        foreach($fields as $key => $field) {
            //$this->set_postback_value($field, $model_instance);
            if ($field->field_type == 'tags') {
                //todo: refactor
                $tag_names = $searchparams[$field->field_name]??[];                
                $ids_tags = [];
                if (!empty($tag_names)) {
                    foreach($tag_names as $tag_name)
                    {
                        $tag = \Spatie\Tags\Tag::findFromString($tag_name, $this->type);       
                        if ($tag != null) {
                            $ids_tags[] = $tag->id;    
                        }
                    }
                }
                $field->postback_value = $ids_tags;
                $field->set_data(\Spatie\Tags\Tag::withType($this->type)->get()); 
            } else {
                $field->postback_value = $searchparams[$field->field_name]??'';                
            }
            if ( ($field->render_type == 'checkbox') && !$field->is_collection()) {                
                $options = [];
                $option = [
                    'value' => '0',
                    'label' => 'Nee',
                ];
                $options[] = (object)$option;
                $option = [
                    'value' => '1',
                    'label' => 'Ja',
                ];
                $options[] = (object)$option;
                $field->set_data($options);
                $field->render_type_search = 'select';
            }
            // if ($field->field_name == 'brand_id') {                
            //     dd($field);
            // }
            $fields[$key] = $field;
        }
        $template = 'content.index_searchform';    

        $viewdata = [
            'fields' => $fields,
            'searchparams' => $searchparams,
        ];
        if (!empty($fields)) {
            $view = View::make($template, $viewdata);        
            $html = $view->render();    
        }
        return $html;
    }
}