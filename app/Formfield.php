<?php

namespace App;

use View;
use Illuminate\Database\Eloquent\Model;

class Formfield extends Model
{
    protected $fillable = ['title', 'field_name', 'render_type', 'field_type', 'data_value', 'form_id'];    

    public function form() {
        return $this->belongsTo('App\Form');
    }

    public function contentfield()
    {
        return new contentfield([
            'name' => $this->title,
            'field_name' => $this->field_name,
            'render_type' => $this->render_type,
            'field_type' => $this->field_type,
            'data_value' => $this->data_value,
        ]);
    }
}
