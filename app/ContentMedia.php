<?php
namespace App;

class ContentMedia {
    public $name;
    public $title;
    public $heading;
    public $properties;
    
    public function __construct(array $media) {
        $this->name = $media['name'] ?? null;
        $this->title = $media['title'] ?? null;
        $this->heading = $media['heading'] ?? 0;
        $this->properties = $media['properties'] ?? [];
        if ($this->heading != 1) {
            $this->heading = 0;
        }
    }

    public function get_heading_properties() 
    {
        $items = collect($this->properties)->filter(function ($value, $key) {
            dd($value);
            return ($value['name'] == $collection_name);
        });
        return $items->toArray();
    }
}