<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class menu_items extends Model {
    //
    protected $fillable = ["id","title", 'action', 'url', 'new_window'];

}
