<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    //protected $fillable = ["id","title", "intro", "message_ok"];
    protected $guarded = ['id'];

    public function fields()
    {
        return $this->hasMany(FormField::class);
    }

    public function render_fields()
    {
        $data = '';
        foreach($this->fields as $field){
            $template = '';
            // $custom_template = 'cmsextra.'.strtolower($this->get('model')).'.'.$field->field_name;
            // if (View::exists($custom_template)){
            //     $template = $custom_template;
            // }    
            $data .= $field->contentfield()->render($template);
        }
        return $data;
    }

    public function render()
    {
        $viewdata = [
            'form' => $this,
            'fields_html' => $this->render_fields()
        ];
        return view('forms.form', $viewdata);
    }
}
