<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\ContentType;
use Route;

class ContentComposer
{
    private $contentType;
    private $model;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(Request $request)
    {
        if ($request->route() == null) {
            return; //nodig voor php artisan route:list, anders werkt dit neit error
        }
        $type = $request->route()->parameter('type');

        $this->contentType = new ContentType($type);
        $this->model = $this->contentType->getModelName();    
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $viewdata = [
            'contentType' => $this->contentType,
            'pk' => $this->contentType->getPk()
        ];
        $view->with($viewdata);
    }

    private function is_edit_page()
    {
        $currentRoute = Route::current();
        $id = $currentRoute->parameter('content', 0);   
        return ($id > 0);     
    }

    public function compose_menu(View $view) 
    {
        $current_route_url = \Request::url();
        $current_route_name = Route::currentRouteName();

        $currentRoute = Route::current();
        $id = $currentRoute->parameter('content', 0);
        $menu_items = get_content_menu($id, $this->contentType);
        $viewdata = [
            'menu_items' => $menu_items
        ];
        $view->with($viewdata);
    }
}