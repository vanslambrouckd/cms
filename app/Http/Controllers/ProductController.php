<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Brand;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Http\Requests;
use App\ContentType;
use App\ContentRelation;

class ProductController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products = Product::all();
        $viewdata = [
            'products' => $products,        
        ];

        return view('products.index', $viewdata);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::all();
        $product = new Product();

        $viewdata = [
            'product' => $product,
            'brands' => $brands,
        ];
        return view('products.create', $viewdata);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        Product::create($request->all());
        return redirect()->route('products.index')->with('success', 'Product has been added');
        //return back()->with('success', 'Product has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    public function mediaIndex(Product $product)
    {
        $viewdata = [
            'product' => $product,
            'images' => $product->getMedia('test')
        ];

        return view('products.media', $viewdata);
    }

    public function mediaStore(Product $product, Request $request) {
        $product->addMedia($request->file)
        ->toMediaCollection('test');

        return redirect()->route('products.media.index', ['id' => $product->id])->with('success','The media has been added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $brands = Brand::all();

        $product->brand = $product->brand ?? new Brand();

        $viewdata = [
            'product' => $product,
            'brands' => $brands,
        ];

        return view('products.edit', $viewdata);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    //public function update(Request $request, Product $product)
    {
        // if (class_exists('App\Http\Requests\ProductRequest')) {
        //     $request = new ProductRequest();
        // }

        // $request->rules = [
        //     'title' => 'required',
        // ];

        //dd($request);
        //dd(get_class($request));
        $product->update($request->all());
        return back()->with('success','Product has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
        $product->delete();
        return redirect()->route('products.index')->with('success','Product has been deleted');        
    }

    public function mediaDestroy(Product $product, $mediaId)
    {
        //
        $product->getMedia('test')
        ->keyBy('id')
        ->get($mediaId)
        ->delete();

        return back()->with('success','Product has been deleted');
    }

    public function categoriesIndex(Product $product)
    {
        $categories = Category::all();
        $prodCatIds = $product->categories->pluck('id')->toArray();

        $viewdata = [
            'product' => $product,
            'categories' => $categories,
            'prodCatIds' => $prodCatIds,
        ];

        return view('products.categories', $viewdata);
    }

    public function categoriesStore(Product $product, Request $request)
    {
        $product->categories()->sync($request->categories);
        return back()->with('success','The data has been saved');
    }

    private function group_specs_by_tag($specifications)
    {
        $field_name = 'specifications';

        $specs_per_tag = [];

        $tags = \Spatie\Tags\Tag::withType($field_name)->get();
        foreach($tags as $tag) {
            $specs_per_tag[$tag->id] = [
                'name' => $tag->name,
                'data' => []
            ];
        }

        if (count($specifications)) {
            foreach($specifications as $spec) {
                foreach($spec->tags as $t) {
                    $specs_per_tag[$t->id]['data'][$spec->id] = $spec;
                }
            }
        }
        //hier doordoen
        return $specs_per_tag;
    }

    public function specificationsIndex(int $id) {

        $contentType = new ContentType('products');
        $menu_items = get_content_menu($id, $contentType);

        $field_name = 'specifications';
        $rel = $contentType->get_relation_field_by_fieldname($field_name);
        $relation = new ContentRelation((array)$rel);

        $product = Product::findOrFail($id);
        $items_per_tag = $this->group_specs_by_tag($product->specifications);

        $values_selected = [];
        foreach($product[$relation->field_name] as $spec) {
            $values_selected[$spec->id] = $spec;
        }

        $viewdata = [
            'relation' => $relation,
            'product' => $product,
            'values_selected' => $values_selected,
            'field_name' => $field_name,
            'items_per_tag' => $items_per_tag,
            'contentType' => $contentType,
            'menu_items' => $menu_items,
        ];
        return view('products.specifications_by_tag', $viewdata);
    }

    public function specifications_store(int $id, Request $request) 
    {
        $item = Product::findOrFail($id);
        $item->specifications()->sync($request->relation);
        
        return back()->with('success','The data has been saved');
    }
}
