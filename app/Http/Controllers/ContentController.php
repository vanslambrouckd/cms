<?php

namespace App\Http\Controllers;

use DB;
use Log;
use App\ContentType;
use App\ContentField;
use App\ContentRelation;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Requests\ContentRequest;
use Route;
use App;
use Spatie\MediaLibrary\Media;
use View;
//use App\Http\Controllers\ProductController;

class ContentController extends BaseController
{
    private $contentType;
    private $model;

    public function __construct(Request $request) {
        parent::__construct();
        if ($request->route() == null) {
            return; //nodig voor php artisan route:list, anders werkt dit neit error
        }

        $type = $request->route()->parameter('type');
        $this->contentType = new ContentType($type);
        $this->model = $this->contentType->getModelName();    
        //dd($this->model);
    }  

    public function filter(Request $request)
    {
        //doordoel filter
        // echo '<pre>';
        // print_r($request->all());
        // echo '</pre>';
        $searchdata = [];
        $fields_searchable = $this->contentType->fields_searchable();
        foreach($fields_searchable as $field) {
            if ($request->has($field->field_name)) {
                $searchval = $request->input($field->field_name);
               if ($searchval != '') {
                    $searchdata[$field->field_name] = $searchval;                    
                }
            }
        }
        $sessionkey = $this->get_session_key_search();

        $request->session()->put($sessionkey, $searchdata);
        
        return redirect()->route('content.index', ['type' => $this->contentType->type]);
    }

    private function get_searchparams() {
        $request = \Request::instance();
        $searchparams = $request->session()->get($this->get_session_key_search(), []);
        return $searchparams;
    }

    public function getIndexQueryBuilder() {
        // $tmp = DB::table('products')->get();
        // dd($tmp);

        // $owners = App\Review::join('products', 'products.id', '=', 'reviews.product_id')->get();
        // dd($owners);

        //$tmp = Product::has('reviews')->with('reviews')->get();
        // $tmp = Product::whereHas('reviews', function($query) {
        //     return $query->whereIn('id', [1,2]);
        // })->with('reviews')->get();
        // dd($tmp);

        // $tmp = Product::with('reviews')->whereHas('reviews', function($query) {
        //     return $query->where('id', 1)->orderBy('id', 'desc');
        // })->get();
        // dd($tmp);

        // $prod = Product::find(3);
        // $prod->attachTag('tag4');
        // dd($prod);
        $searchparams = $this->get_searchparams();
        $searchdata = [];
        $fields_searchable = $this->contentType->fields_searchable();
        foreach($fields_searchable as $field) {
            if (isset($searchparams[$field->field_name])) {                
                // dd($field);
                // dd($searchparams);
                $searchval = $searchparams[$field->field_name];
                if ($searchval != '') {
                    $searchdata[$field->field_name] = $searchval;                    
                }
            }
        }

        $query = $this->model::query();
        $where = [];
        foreach($searchdata as $key => $searchval) {
            $field = $this->contentType->get_content_field_by_fieldname($key);
            //todo: refactor
            if ($field->field_type == 'tags') {
                $query = $query->withAnyTags($searchval, $this->contentType->type);
            } elseif ($field->relation == 'hasMany') {
                if (!is_array($searchval)) {
                    $searchval = [$searchval];
                }
                $query = $query->whereHas($field->field_name, function($query) use ($field, $searchval) {
                    return $query->whereIn($field->data_value, $searchval);
                });
            } elseif ($field->relation == 'hasOne') {
                if (!is_array($searchval)) {
                    $searchval = [$searchval];
                }
                $query = $query->whereIn($field->field_name, $searchval);
            } else {
                $where[] = [$key, 'like', '%'.$searchval.'%'];                
            }
        }
        if (!empty($where)) {
            $query = $query->where($where);            
        }

        return $query;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchparams = $this->get_searchparams();
        //todo: eager load images in overview
        $order_field = $request->get('order_field', $this->contentType->get('default_sortfield', 'id'));
        $order = $request->get('order', $this->contentType->get('default_sortorder', 'desc'));
        $viewdata = [
            'images' => $this->contentType->images_heading(),
            'fields' => $this->contentType->fields_heading([]),
            'order' => $order,
            'pagination_appends' => [
                'order_field' => $order_field,
                'order' => $order,
            ]
        ];

        $b_is_hierarchy = $this->contentType->is_hierarchy();
        if ($b_is_hierarchy) {
            $items = $this->model::getHierarchy();
            $tpl = 'content.index_hierarchy';
        } else {
            $items = $this->getIndexQueryBuilder();
            //$items = $items->orderBy($order_field, $order)->get();
            $paginate = $this->contentType->get('paginate', null);
            if (!empty($paginate)) {
                $items = $items->orderBy($order_field, $order);
                Log::info($items->toSql());
                $items = $items->paginate($paginate);
            } else {
                //$items = $this->model::All();
                $items = $items->orderBy($order_field, $order)->get();
            }

            $tpl = 'content.index';
            $custom_template = 'cmsextra.'.$this->contentType->get_model_folder().'.index';
            if (View::exists($custom_template)){
                $tpl = $custom_template;
            }  
        }

        $viewdata['searchfilters_active'] = $this->contentType->get_active_searchfilters($searchparams);


        $viewdata['contentType'] = $this->contentType;
        $viewdata['items'] = $items;
        $viewdata['searchparams'] = $searchparams;
        $viewdata['has_searchable_fields'] = !empty($this->contentType->fields_searchable());

        $viewdata['has_pagination'] = false; //$this->contentType->has_pagination();
        return view($tpl, $viewdata);
    }
    public function get_url_overview() {
        return route('content.index', ['type' => $this->contentType->type]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(string $type)
    {
        $item = new $this->model;

         $this->contentType->set_postback_values($item); //todo: refactor moet ook voor de create functie toegevoegd worden!!        
        $url_overview = route('content.index', ['type' => $this->contentType->type]);

         $viewdata = [
             'item' => $item,
             'fields_html' => $this->contentType->render_fields(),
             'url_overview' => $this->get_url_overview()
         ];
         return view('content.create', $viewdata);   
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //public function store(ContentRequest $request, string $type, int $id)
    public function store(Request $request, string $type)
    {
        $data = $this->get_validated_data($request, $type, 0);
        $model_id = $this->model::create($data)->id;

        $item = $this->model::findOrFail($model_id);
        $this->update_all_has_many_fields($request, $item);
        $this->save_field_tags($request, $type, $model_id);
        $this->update_hierarchy($request, $model_id);

        return redirect()->route('content.index', ['type' => $this->contentType->type])->with('success','The data has been saved');        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    public function search_clear_all(Request $request, string $type) 
    {
        $sessionkey = $this->get_session_key_search();
        $request->session()->forget($sessionkey);
        return redirect()->route('content.index', ['type' => $this->contentType->type])->with('success','The data has been saved');                
    }

    public function get_session_key_search() {
        $sessionkey = 'content.search.'.$this->contentType->type;
        return $sessionkey;
    }

    public function search_remove_field(Request $request, string $type, string $contentfield_name) 
    {
        $field = $this->contentType->get_content_field_by_fieldname($contentfield_name);
        $searchparams = $this->get_searchparams();   

        $sessionkey = $this->get_session_key_search().'.'.$contentfield_name;

        if ($request->session()->has($sessionkey)) {
            $request->session()->forget($sessionkey);
        }
        return redirect()->route('content.index', ['type' => $this->contentType->type])->with('success','The data has been saved');        
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, string $type, int $id)
    {
        $item = $this->model::findOrFail($id);

         $this->contentType->set_postback_values($item); //todo: refactor moet ook voor de create functie toegevoegd worden!!        
         $viewdata = [
           'item' => $item,
           'fields_html' => $this->contentType->render_fields(),
           'url_overview' => $this->get_url_overview()
       ];
       return view('content.edit', $viewdata);
   }

   private function save_field_tags(Request $request, $type, int $model_id) {
    $item = $this->model::findOrFail($model_id);
    $fields_tags = $this->contentType->fields_tags();
    if (count($fields_tags) > 0) 
    {
        foreach($fields_tags as $fld) {
            $tags = $request->get($fld->field_name);
            if (!empty($tags)) {
                foreach($tags as $tag_name) {
                    $tag = \Spatie\Tags\Tag::findOrCreate($tag_name, $type);
                }
            }
            $item->syncTagsWithType($tags, $type);
        }
    }
}

private function update_hierarchy(Request $request, int $model_id)
{
    $item = $this->model::findOrFail($model_id);
    if ($this->contentType->is_hierarchy())
    {
        $hierarchy_field = $this->contentType->get_hierarchy_field();
        $parent_id = $request->get($hierarchy_field->field_name, null);
        if (!empty($parent_id)) {
            $parent = $this->model::find($parent_id);
            $item->makeChildOf($parent);
        }
    }
}

    public function get_validated_data(Request $request, string $type, $id)
    {
        $req = App::make('App\Http\Requests\ContentRequest', ['type' => $type, 'id' => $id]); //zie CmsServiceProvider.php   
        $refl = new \ReflectionClass($req);
        $classname = $refl->getShortName();
        if ($classname == 'ContentRequest') {
            //dd($req->rules());
            $data = $request->validate($req->rules());            
        } else {
            $data = $request->all();
        }

        return $data;
    }

    private function update_has_many_field(Request $request, $item, ContentField $field)
    {
        $field_name = $field->field_name;
        $relation = new ContentRelation([
            'name' => $field->name,
            'field_name' => $field->field_name,
            'field_type' => $field->relation,
            'field_type' => $field->field_type,
            'data_value' => $field->data_value,
            'render_type' => $field->render_type,
            'related_class' => $field->related_class,
            'related_field' => $field->related_field,
        ]);

        $related_class_model = $relation->related_class;
        $related_class_model::where($relation->related_field, $item->id)->update([$relation->related_field => null]);

        $ids = $request->get($field_name, []);
        if (!empty($ids)) {
                    //gekozen items linken
            foreach($ids as $id) {
                $related_item = $related_class_model::find($id);
                        //dd($related_item);
                $related_item->{$relation->related_field} = $item->{$this->contentType->getPk()};
                $related_item->save();
            }
        }
    }

    private function update_all_has_many_fields(Request $request, $item)
    {
        $has_many_fields = $this->contentType->get_has_many();
        if (!empty($has_many_fields)) {
            foreach($has_many_fields as $field) {
                $this->update_has_many_field($request, $item, $field);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $type, int $id)
    {
        //$req = App::make('App\Http\Requests\ContentRequest', ['type' => $type, 'id' => $id]); //zie CmsServiceProvider.php
       //die('jaa');

        $item = $this->model::findOrFail($id);
        $this->update_all_has_many_fields($request, $item);
        $this->save_field_tags($request, $type, $id);
        $this->update_hierarchy($request, $id);
        $data = $this->get_validated_data($request, $type, $id);
        $item->update($data);
        return back()->with('success','The data has been updated');
    }

    public function mediaIndex(string $type, int $id, string $collection_name)
    {
        $request = \Request::instance();
        $response_type = $request->get('response_type', 'html');

        $item = $this->model::findOrFail($id);
        $current_media_headings = $this->contentType->current_mediacollection_headings($collection_name);
        $images = $item->getMedia($collection_name);
        $images = $images->reverse()->values();

        foreach($images as $image) {
            if (in_array($image->mime_type, ['image/jpeg', 'image/png'])) {
                $image->image_url = '<img src="'.$image->getUrl().'" class="img-thumbnail" />';
            } else {
                $image->image_url = '';
            }

            $image->url_sort_up = action('ContentController@mediaSort', ['type' => $this->contentType->type, 'id' => $item->id, 'collection_name' => $collection_name, 'media_id' => $image->id, 'direction' => 'up']);
            $image->url_sort_down = action('ContentController@mediaSort', ['type' => $this->contentType->type, 'id' => $item->id, 'collection_name' => $collection_name, 'media_id' => $image->id, 'direction' => 'down']);

            $image->url_media_edit = action('ContentController@mediaMetaEdit', ['type' => $this->contentType->type, 'id' => $item->id, 'collection_name' => $collection_name, 'media_id' => $image->id]);
            $image->url_media_delete = action('ContentController@mediaDestroy', ['type' => $this->contentType->type, 'id' => $item->id, 'collection_name' => $collection_name, 'media_id' => $image->id]);
        }

        $viewdata = [
            'item' => $item,
            'fields' => $this->contentType->fields_edit(),
            'media' => $this->contentType->media(),
            'current_media_headings' => $current_media_headings,
            'show_edit' => !empty($current_media_headings),
            'images' => $images,      
            'collection_name' => $collection_name,     
        ];

        if ($response_type == 'json') {            
            return $viewdata;
        } else {
            return view('content.media', $viewdata);            
        }
    }

    public function mediaStore(Request $request, string $type, int $id, string $collection_name) {
        $item = $this->model::findOrFail($id);

        $item->addMedia($request->file)
        ->toMediaCollection($collection_name);

        //return redirect()->route('content.media.index', ['type' => $this->contentType->type, 'id' => $item->id, 'collection_name' => $collection_name])->with('success','The media has been added');
    }

    public function mediaDestroy(string $type, int $id, string $collection_name, int $mediaId)
    {
        $item = $this->model::findOrFail($id);

        $item->getMedia($collection_name)
        ->keyBy('id')
        ->get($mediaId)
        ->delete();

        //return back()->with('success','Item has been deleted');
    }

    public function destroy(string $type, int $id)
    {
        $item = $this->model::findOrFail($id);
        $item->delete();
        return redirect()->route('content.index', ['type' => $this->contentType->type])->with('success','Item has been deleted');        
    }

    public function relations_hasmany(string $type, int $id, $field_name) 
    {
        $rel = $this->contentType->get_relation_field_by_fieldname($field_name);
        $relation = new ContentRelation((array)$rel);

        $item = $this->model::findOrFail($id);
        
        $viewdata = [
            'item' => $item,
            'field_name' => $field_name,
            'relation_html' => $relation->render($item),
            'url_overview' => $this->get_url_overview(),
        ];

        return view('content.relations_hasmany', $viewdata);
    }

    public function relations_hasmany_store(string $type, int $id, $field_name, Request $request) 
    {
        // echo 'type='.$type;
        // echo '<br />';
        // echo 'id='.$id;
        // echo '<br />';
        // echo 'field_name='.$field_name;
        // dd($request->all());
        // die('jaa');
        $item = $this->model::findOrFail($id);        

        $field = $this->contentType->get_relation_field_by_fieldname($field_name);    
        $this->update_has_many_field($request, $item, $field);

        return back()->with('success','The data has been saved');
    }


    public function relations_belongstomany(string $type, int $id, $field_name) 
    {
        // echo 'type='.$type;
        // echo '<br />';
        // echo 'id='.$id;
        // echo '<br />';
        // echo 'field_name='.$field_name;
        // die();

        $rel = $this->contentType->get_relation_field_by_fieldname($field_name);
        $relation = new ContentRelation((array)$rel);

        //dd($relation->data());

        $item = $this->model::findOrFail($id);
        //dd($relation);
        $values_selected = $item[$relation->field_name]->pluck($relation->data_value)->toArray();

        $viewdata = [
            'relation' => $relation,
            'item' => $item,
            'values_selected' => $values_selected,
            'data' => $relation->data($item),
            'field_name' => $field_name,
            'url_overview' => $this->get_url_overview()
            // 'belongs_to_many' => $this->contentType->get_belongs_to_many(),
        ];

        return view('content.relations_belongstomany', $viewdata);
    }

    public function relations_belongstomany_by_tag(string $type, int $id, string $field_name)
    {
        //dd($field_name);
         /*
        indien er een action veld in de config file is, moet je de controller@action oproepen van het action veld ipv de huidige controller action
        dit is om de huidige belongstomany versie te overschrijven met een eigen gemaakte versie
        */
        $rel = $this->contentType->get_relation_field_by_fieldname($field_name);
        $act = $rel->action??'';
        $arr = get_controller_and_action_from_string($act);

        if (!empty($arr)) {
            $relation = new ContentRelation((array)$rel);
            $items_per_tag = $this->contentType->group_data_by_tag($relation, $field_name);

            $item = $this->model::findOrFail($id);

            $ret = (new ProductController())->specificationsIndex($type, $id, $field_name); //todo:variabel maken

            //$ret = (new $controller())->specificationsIndex($type, $id, $field_name);
            return $ret->render();
        } else {
            $rel = $this->contentType->get_relation_field_by_fieldname($field_name);
            $relation = new ContentRelation((array)$rel);
        //dd($relation);

            $item = $this->model::findOrFail($id);

            $items_per_tag = $this->contentType->group_data_by_tag($relation, $field_name, $item);
        //dd($relation);
            $values_selected = $item[$relation->field_name]->pluck($relation->data_value)->toArray();

            $viewdata = [
                'relation' => $relation,
                'item' => $item,
                'values_selected' => $values_selected,
            //'data' => $data,
                'field_name' => $field_name,
                'items_per_tag' => $items_per_tag,
            // 'belongs_to_many' => $this->contentType->get_belongs_to_many(),
            ];

            return view('content.relations_belongstomany_by_tag', $viewdata);
        }
    }

    public function relations_belongstomany_store(string $type, int $id, $field_name, Request $request) 
    {
        // echo 'type='.$type;
        // echo '<br />';
        // echo 'id='.$id;
        // echo '<br />';
        // echo 'field_name='.$field_name;
        // die();

        $item = $this->model::findOrFail($id);
        $item->{$field_name}()->sync($request->relation);
        return back()->with('success','The data has been saved');
    }

    public function mediaMetaEdit(string $type, int $id, string $collection_name, int $mediaId) {
        $item = $this->model::findOrFail($id);
        $media_item = $item->getMedia($collection_name)
        ->keyBy('id')
        ->get($mediaId);
        //dd($media_item);

        $media_properties = $this->contentType->current_mediacollection_properties($collection_name);

        //dd($media_properties);

        $viewdata = [
            'item' => $item,
            'collection_name' => $collection_name,
            'mediaItem' => $media_item,
            'mediaProperties' => $media_properties
        ];
        return view('content.media_meta_edit', $viewdata);
    }

    public function mediaMetaUpdate(Request $request, string $type, int $id, string $collection_name, int $mediaId) 
    {
        $item = $this->model::findOrFail($id);
        $media_item = $item->getMedia($collection_name)
        ->keyBy('id')
        ->get($mediaId);

        $properties = $request->get('properties', []);
        if (!empty($properties)) {
            foreach($properties as $property => $val) {
                $media_item->setCustomProperty($property, $val)->save();
            }
        }
        return redirect()->route('content.media.index', ['type' => $this->contentType->type, 'id' => $item->id, 'collection_name' => $collection_name])->with('success','The media properties have been saved');
    }

    private function hierarchy_get_previous_sibling($sibling_id_current, $siblings)
    {
        //todo: functie verplaatsen, mag niet in controller
        $prev_sibling = null;
        foreach($siblings as $key => $sibling) {
            if ($sibling->id == $sibling_id_current) {
                $prev_sibling = isset($siblings[$key-1])?$siblings[$key-1]:null;
            }
        }
        return $prev_sibling;
    }

    private function hierarchy_get_next_sibling($sibling_id_current, $siblings)
    {
        //todo: functie verplaatsen, mag niet in controller
        $prev_sibling = null;
        foreach($siblings as $key => $sibling) {
            if ($sibling->id == $sibling_id_current) {
                $prev_sibling = isset($siblings[$key+1])?$siblings[$key+1]:null;
            }
        }
        return $prev_sibling;
    }

    public function indexSort(Request $request, string $type, int $id, string $direction) 
    {
        //todo: refactor
        $item = $this->model::findOrFail($id);
        $siblings = $item->getSiblingsAndSelf();

        if ($direction == 'left') {
            $parent = $item->parent;
            if (!empty($parent)) {
                $item->makeSiblingOf($parent);
            }
        } elseif ($direction == 'right') {
            $prev_sibling = $this->hierarchy_get_previous_sibling($id, $siblings);
            if (!empty($prev_sibling)) {
                $item->makeChildOf($prev_sibling);
            }
        } elseif ($direction == 'up') {
            $prev_sibling = $this->hierarchy_get_previous_sibling($id, $siblings);
            if (!empty($prev_sibling)) {
                $item->makePreviousSiblingOf($prev_sibling);
            }
        } elseif($direction == 'down') {
            $next_sibling = $this->hierarchy_get_next_sibling($id, $siblings);
            if (!empty($next_sibling)) {
                $item->makeNextSiblingOf($next_sibling);
            }
        }
        return redirect()->route('content.index', ['type' => $this->contentType->type])->with('success','The data has been saved');
    }

    public function mediaSort(Request $request, string $type, int $id, string $collection_name, int $mediaId, string $direction) {
        echo $type;
        echo '<br />';
        echo $id;
        echo '<br />';
        echo $collection_name;
        echo '<br />';
        echo $mediaId;        
        echo '<hr />';
        $item = $this->model::findOrFail($id);
        $images = $item->getMedia($collection_name);

        switch($direction) {
            case 'down':
            foreach($images as $key => $image) {
                if ($image->id == $mediaId) {
                    $item_prev = isset($images[$key-1])?$images[$key-1]:null;
                    if (!empty($item_prev)) {
                                //switch positions
                        $images[$key-1] = $image;
                        $images[$key] = $item_prev;
                    }
                }
            }
            break;

            case 'up':
            foreach($images as $key => $image) {
                if ($image->id == $mediaId) {
                    $item_next = isset($images[$key+1])?$images[$key+1]:null;
                    if (!empty($item_next)) {
                                //switch positions
                        $images[$key+1] = $image;
                        $images[$key] = $item_next;
                    }
                }
            }
            break;
        }
        
        
        $ids = $images->pluck('id')->toArray();
        Media::setNewOrder($ids);

        //return redirect()->route('content.media.index', ['type' => $this->contentType->type, 'id' => $item->id, 'collection_name' => $collection_name])->with('success','The media has been sorted');
    }
}
