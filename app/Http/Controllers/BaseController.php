<?php
namespace App\Http\Controllers;
use App\ContentType;

class BaseController extends Controller
{
    protected $contentTypes;

    public function __construct() {
        $this->contentTypes = self::readContentTypes();
    }

    private function readContentTypes(): array 
    {
        $contentTypes = [];
        $files = config('cms.contenttypes');

        if (!empty($files)) {
            foreach($files as $type => $data) {
                $contentTypes[$type] = $data;
                $c = new ContentType($type); //zodat ContentType->createFile opgeroepen wordt, om niet bestaande model files te creeren
            }
        }        
        return $contentTypes;
    }
}
