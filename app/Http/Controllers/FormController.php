<?php

namespace App\Http\Controllers;

use App;
use App\Form;
use App\ContentType;
use App\ContentField;
use App\Http\Requests\ContentRequest;
use App\Formfield;
use Illuminate\Http\Request;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function fieldsIndex(Form $form)
    {
        $contentType = new ContentType('form');
        $menu_items = get_content_menu($form->id, $contentType); //todo
        $formfields = $form->fields;
        // foreach($formfields as $fld){
        //     if ($fld->field_name == "product")
        //     {
        //     }
        // }

        $viewdata = [
            'contentType' => $contentType,
            'form' => $form,
            'fields' => $formfields,
            'menu_items' => $menu_items,
        ];
        return view('forms.fields.index', $viewdata);
    }

    public function get_validated_data(Request $request, string $type, $id)
    {
        $req = App::make('App\Http\Requests\ContentRequest', ['type' => $type, 'id' => $id]); //zie CmsServiceProvider.php   
        $refl = new \ReflectionClass($req);
        $classname = $refl->getShortName();
        if ($classname == 'ContentRequest') {
            //dd($req->rules());
            $data = $request->validate($req->rules());            
        } else {
            $data = $request->all();
        }

        return $data;
    }

    public function fieldsCreate(Form $form)
    {
        $menu_items = get_content_menu($form->id, new ContentType('forms')); //todo

        $contentType = new ContentType('formfields');

        $viewdata = [
           'form' => $form,            
           'menu_items' => $menu_items,
           'contentType' => $contentType,            
           'fields_html' => $contentType->render_fields(),
       ];

       return view('forms.fields.create', $viewdata);
   }

   public function fieldsEdit(Form $form, int $formfield_id)
   {
        $item = Formfield::findOrFail($formfield_id); //dependency injection van model instance werkt blijkbaar niet..., dus op deze manier

        $menu_items = get_content_menu($form->id, new ContentType('forms')); //todo

        $contentType = new ContentType('formfields');
        $contentType->set_postback_values($item);
        //dd($contentType->fields);
        $viewdata = [
            'form' => $form,
            'menu_items' => $menu_items,
            'contentType' => $contentType,
            'fields_html' => $contentType->render_fields(),            
            'item' => $item,
        ];

        return view('forms.fields.edit', $viewdata);
    }

    public function fieldsUpdate(Request $request, Form $form, int $formfield_id)
    {
        //
        $formfield = Formfield::findOrFail($formfield_id);
        $data = $request->all();
        $formfield->update($data);
        return redirect()->route('content.forms.fields.index', ['form' => $form])->with('success','The field has been added');
    }

    public function fieldsStore(Request $request, Form $form)
    {
        //todo: validation
        $data = $this->get_validated_data($request, 'formfields', 0);
        $model_id = $formfield = Formfield::create($data)->id;
        dd($model_id);

        $data = $request->all();
        $data['form_id'] = $form->id;
        $field = Formfield::create($data);
        return redirect()->route('content.forms.fields.index', ['form' => $form])->with('success','The field has been added');
    }

    public function fieldsDestroy(Form $form, int $formfield_id)
    {
        $item = Formfield::findOrFail($formfield_id);
        $item->delete();
        return redirect()->route('content.forms.fields.index', ['form' => $form])->with('success','The field has been added');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function show(Form $form)
    {
        //
    }

    public function preview(Form $form)
    {
        //
        return $form->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function edit(Form $form)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Form $form)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function destroy(Form $form)
    {
        //
    }
}