<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use App\ContentField;
use Response;
use Config;

class CmsBuildController extends Controller
{
    //
    public function index()
    {
        $files = $this->read_configs();
        $viewdata = [
            'files' => $files
        ];

        return view('builder.index', $viewdata);
    }

    public function edit(string $filename) 
    {
        $filename = str_replace('.php', '', $filename);
        $data = config('cms.contenttypes.'.$filename);

        $rendertypes = [
            '' => '',
            'textfield' => 'textfield',
            'textarea' => 'textarea',
            'select' => 'select',
            'select2' => 'select2',
            'checkbox' => 'checkbox',
        ];

        $fieldtypes = [
            '',
            'hasMany',
            'belongsToMany', 
            'hierarchy', 
            'json', 
            'custom', 
            'boolean', 
            'query', 
            'tags'
        ];

        $relations = [
            '',
            'hasOne',
            'hasMany',
        ];

        $related_classes = $data_classes = $this->read_data_classes();
        //dd($data_classes);
        //$data_functions = $this->get_object_methods('Product');
        
        $viewdata = [
            'data' => $data,
            'rendertypes' => $rendertypes,
            'fieldtypes' => $fieldtypes,
            'data_classes' => $data_classes,
            'related_classes' => $related_classes,
            'relations' => $relations,
            'filename' => $filename,
            //'data_functions' => $data_functions,
        ];


        // dd($data);
        return view('builder.edit', $viewdata);
    }

    public function store(Request $request, string $filename)
    {
        //todo: doordoen (vervangen door laravel config writer (https://github.com/axdlee/laravel-config-writer))
        $fields = $request->all('fields');

        config::set('cms.contenttypes.'.$filename.'.fields', $fields);
        $data = config('cms.contenttypes.'.$filename);

        $path = config_path().'/cms/contenttypes/test.php';

        $new = var_export($data, true);
        $new = "<?php
        use Illuminate\Validation\Rule;


        return [
            ".$new."
        ];";
        $new = str_replace('array (', '[', $new);
        $new = str_replace('),', '],', $new);
        File::put($path, $new);
        dd($path);
        dd($data);

        dd($fields);
    }

    public function get_object_methods() {
        $request = \Request::instance();
        $classname = $request->get('data_class', '');

        $methods = [];
        try {
            $obj = new $classname();
            $refl = new \ReflectionClass($obj);
            $constr = $refl->getConstructor();
            $ret = $refl->getMethods(\ReflectionMethod::IS_PUBLIC);
            if (!empty($ret)) {
                foreach($ret as $method) {
                    if ($method->class == $refl->getName())
                    {
                        $methods[] = [
                            'id' => $method->name,
                            'text' => $method->name
                        ];
                    }    
                }
            }    
        } catch(\Exception $ex)
        {

        }
        
        return Response::json( $methods );
    }

    private function read_data_classes() 
    {
        $path = app_path();
        $tmp = File::files($path);
        $files= [];
        if (!empty($tmp))
        {
            foreach($tmp as $item) {
                $filename = str_replace('.php', '', $item->getFilename());
                $files[] = "App\\\\".$filename;
            }
        }
        return $files;
    }

    private function read_configs()
    {
        $files = File::files(config_path().'/cms/contenttypes');
        return $files;
        //dd($files);
    }
}
