<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'price' => "numeric|min:0",
            'price_promo' => "nullable|numeric|min:0",
            'stock' => "nullable|numeric|min:0",
            'description' => 'required',
            'brand_id' => '',
            'secondhand' => '',
            'reviews' => '',
        ];
    }
}
