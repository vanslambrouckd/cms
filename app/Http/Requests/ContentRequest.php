<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContentRequest extends FormRequest
{
    public $content_type = '';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $type = $this->content_type;
        $rules = [];
        $configKey = 'cms.contenttypes.'.$type;
        $fields = config($configKey.'.fields');
        if (!empty($fields)) {
            foreach($fields as $field) {
                $rule = '';
                if (!empty($field['rules'])) {
                    $rule = str_replace("\$id", $this->id, $field['rules']);
                }

                if (strlen(trim($rule)) == 0) {
                    $rule = 'sometimes';
                }
                $rules[$field['field_name']] = $rule;
            }
        }
        //return [];
        //dd($rules);
        return $rules;
    }
}
