<?php
namespace App\Http\Traits;

trait NodeTrait {
    public static function getHierarchy() {
        $hier = [];
        $instance = new static;
        $roots = $instance::roots()->get();
        if (!empty($roots)) {
            foreach($roots as $root) {
                $hier[] = $root;
                $tmp = $root->getDescendants();
                foreach($tmp as $item) {
                    $hier[] = $item;
                }    
            }
        }

        return $hier;
    }
}