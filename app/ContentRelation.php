<?php
namespace App;

use Exception;

class ContentRelation 
{
    public $field_name;
    public $type;
    public $name;
    public $data_class;
    public $data_function;
    public $render_type;
    public $data_value;
    public $data_label;
    public $related_class;
    public $related_field;

    public function __construct($relation) {
        $this->field_name = $relation['field_name'];
        $this->field_type = $relation['field_type'];
        $this->name = $relation['name'];
        $this->render_type = $relation['render_type']??'checkbox';
        $this->data_value = $relation['data_value']??'';
        $this->related_class = $relation['related_class']??''; //voor hasMany
        $this->related_field = $relation['related_field']??''; //voor hasMany
        $this->data_class = $relation['data_class']??'';
        $this->data_function = $relation['data_function']??'';
        $this->data_label = $relation['data_label']??'';

        if (!in_array($this->render_type, ['checkbox', 'radio']))
        {
            throw new Exception('Invalid render type');
        }
    }

    public function data($model_instance)
    {
        $refl = new \ReflectionClass($model_instance);
        $classname = $refl->getShortName();
        
        $data = [];
        switch($this->field_type) {
            case 'belongsToMany':
            //returns Model collection
            $data_class = $this->data_class;
            $data_function = $this->data_function;
            //dd($this);
            $tmp = call_user_func_array(array($data_class, $data_function), array());
            foreach($tmp as $item) {
                $data[$item->{$this->data_value}] = (object)['value' => $item->{$this->data_value}, 'label' => $item->{$this->data_label}];
            }
            break;

            case 'hasMany':
            $data_class = $this->data_class;
            $data_function = $this->data_function;

            if ($classname == 'Form') { //todo: variabel maken
                $tmp = $model_instance->{$data_function};                
            } else {
                $tmp = call_user_func_array(array($data_class, $data_function), array());
            }

            foreach($tmp as $item) {
                $data[$item->{$this->data_value}] = (object)['value' => $item->{$this->data_value}, 'label' => $item->{$this->data_label}];
            }
            break;
        }

        return $data;
    }

    public function render($item)
    {
        $contentfield = new ContentField([
            'name' => $this->name,
            'field_name' => $this->field_name,
            'render_type' => $this->render_type,
            'relation' => $this->field_type,
            'data_class' => $this->data_class,
            'data_function' => $this->data_function,
            'data_value' => $this->data_value,
            'data_label' => $this->data_label,
        ]);
        $contentfield->set_postback_value($item, 0);
        //dd($contentfield->postback_value);
        return $contentfield->render();
    }
}