<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Baum\Node;
use App\Http\Traits\NodeTrait;

class Category extends Node implements HasMedia {
    use HasMediaTrait;
    use NodeTrait;
    
    protected $fillable = ["id","title", 'parent_id'];

    public function products() 
    {
        return $this->belongsToMany(Product::class);
    }
}
