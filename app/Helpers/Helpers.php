<?php
if (!function_exists('get_controller_and_action_from_string'))
{
    function get_controller_and_action_from_string($str) 
    {
        $ret = [];
        $arr = explode('@', $str);
        if (count($arr) == 2) {
            $controller = $arr[0];
            $action = $arr[1];
            $ret['controller'] = $controller;
            $ret['action'] = $action;
        }

        return $ret;
    }
}

if (!function_exists('get_content_menu')) {
    function get_content_menu(int $id_item, App\ContentType $contentType)
    {
        $menu_items = [];

        $current_route_url = \Request::url();
        $current_route_name = Route::currentRouteName();

        $model = $contentType->getModelName();    
        $is_edit_page = ($id_item) > 0?true:false;

        if ($is_edit_page) {
            $item = $model::findOrFail($id_item);
        } else {
            $item = new $model();
        }

        if (!empty($item)) {
            if (!$is_edit_page)
            {
                $url_edit = '#';
            } else {
                $url_edit = action('ContentController@edit', ['type' => $contentType->type, 'id' => $item->id]);  
            }
            $menu_items[] = (object)[
                'url' => $url_edit,
                'title' => 'General',
                'class' => in_array($current_route_name, ['content.edit', 'content.create'])?' active ':'',                
                'cls_disabled' => '',
            ];

            $media = $contentType->media();
            if (!empty($media)) {
                foreach($media as $mediaItem)
                {
                    if ($is_edit_page)
                    {
                        $url_edit = action('ContentController@mediaIndex', ['type' => $contentType->type, 'id' => $item->id, 'collection_name' => $mediaItem->name]);
                    } else 
                    {
                        $url_edit = '#';
                    }

                    $menu_items[] = (object)[
                        'url' => $url_edit,
                        'title' => $mediaItem->title,
                        //'class' => ($url_edit == $current_route_url)?' active ':'',                        
                        'class' => ($url_edit == $current_route_url)?' active ':'',       
                        'cls_disabled' => empty($id)?' menuitem_disabled ':'',                 
                    ];
                }                
            }

            $tabs = $contentType->get_tabs();                
            if (!empty($tabs))
            {
                foreach($tabs as $rel_item) {
                    $rel_item = (object)$rel_item;
                    $relation_type = $rel_item->field_type;
                    switch($relation_type) {
                        case 'custom':
                        if ($is_edit_page) {
                            $url_edit = action($rel_item->action, ['id' => $item->{$contentType->getPk()}]);
                        } else {
                            $url_edit = '#';
                        }
                        

                        $menu_items[] = (object)[
                            'url' => $url_edit,                        
                            'title' => $rel_item->name,
                            'class' => ($url_edit == $current_route_url)?' active ':'',      
                            'cls_disabled' => empty($id)?' menuitem_disabled ':'',                  
                        ];
                        break;
                        case 'belongsToMany':                            
                        $rel_item = (object)$rel_item;
                        if ($is_edit_page) {
                            if (isset($rel_item->group_by_tag) && ($rel_item->group_by_tag)) {
                                $action = 'ContentController@relations_belongstomany_by_tag';
                            } else {
                                $action = 'ContentController@relations_belongstomany';
                            }
                            $url_edit = action($action, ['type' => $contentType->type, 'id' => $item->id, 'belongsToMany' => $rel_item->field_name]);  
                        } else {
                            $url_edit = '#';
                        }

                        $menu_items[] = (object)[
                            'url' => $url_edit,                        
                            'title' => $rel_item->name,
                            'class' => ($url_edit == $current_route_url)?' active ':'',      
                            'cls_disabled' => empty($id)?' menuitem_disabled ':'',                  
                        ];
                        break;

                        case 'hasMany':
                        $rel_item = (object)$rel_item;
                        if ($is_edit_page) {

                            $url_edit = action('ContentController@relations_hasmany', ['type' => $contentType->type, 'id' => $item->id, 'hasMany' => $rel_item->field_name]);
                        } else {
                            $url_edit = '#';
                        }
                        $menu_items[] = (object)[
                            'url' => $url_edit,                        
                            'title' => $rel_item->name,
                            'class' => ($url_edit == $current_route_url)?' active ':'',      
                            'cls_disabled' => empty($id)?' menuitem_disabled ':'',                  
                        ];
                        break;
                    }

                }                
            }
        }
        return $menu_items;
    }
}