<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    //
    protected $fillable = ["id","title"];

    public function products()
    {
        //return $this->belongsTo('App\Product', 'foreign_key');
        return $this->hasMany('App\Product');
    }
}
