# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.20)
# Database: cms
# Generation Time: 2017-11-30 13:32:49 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table brands
# ------------------------------------------------------------

DROP TABLE IF EXISTS `brands`;

CREATE TABLE `brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `brands_title_unique` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;

INSERT INTO `brands` (`id`, `title`, `created_at`, `updated_at`)
VALUES
	(1,'Apple',NULL,NULL),
	(2,'Samsung',NULL,NULL),
	(3,'Motorola',NULL,NULL),
	(4,'Nokia',NULL,NULL),
	(5,'Huawei',NULL,NULL);

/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_title_unique` (`title`),
  KEY `categories_parent_id_index` (`parent_id`),
  KEY `categories_lft_index` (`lft`),
  KEY `categories_rgt_index` (`rgt`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `title`, `parent_id`, `lft`, `rgt`, `depth`, `created_at`, `updated_at`)
VALUES
	(5,'TV & Audio',6,2,3,1,'2017-11-12 12:27:48','2017-11-30 11:29:44'),
	(6,'Witgoed & huishouden',NULL,1,10,0,'2017-11-12 12:27:48','2017-11-30 11:29:52'),
	(7,'Koken & wonen',6,4,5,1,'2017-11-12 12:27:48','2017-11-30 11:29:49'),
	(8,'Foto & video',6,6,7,1,'2017-11-12 12:27:48','2017-11-30 11:29:50'),
	(9,'Navigatie & reizen',6,8,9,1,'2017-11-12 12:27:48','2017-11-30 11:29:52');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table category_product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `category_product`;

CREATE TABLE `category_product` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `category_product` WRITE;
/*!40000 ALTER TABLE `category_product` DISABLE KEYS */;

INSERT INTO `category_product` (`product_id`, `category_id`)
VALUES
	(1,6),
	(1,8),
	(2,7),
	(3,9),
	(4,7),
	(5,5),
	(6,2),
	(7,5),
	(8,2),
	(9,3),
	(10,1);

/*!40000 ALTER TABLE `category_product` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table countries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `countries_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;

INSERT INTO `countries` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'Argentina','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(2,'Bermuda','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(3,'Zambia','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(4,'Oman','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(5,'Iran','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(6,'Sao Tome and Principe','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(7,'Haiti','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(8,'Colombia','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(9,'Israel','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(10,'Congo','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(11,'Gabon','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(12,'Somalia','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(13,'Morocco','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(14,'Suriname','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(15,'Marshall Islands','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(16,'Ethiopia','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(17,'Swaziland','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(18,'Honduras','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(19,'Saint Helena','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(20,'Uganda','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(21,'Czech Republic','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(22,'Luxembourg','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(23,'Jersey','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(24,'Fiji','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(25,'Holy See (Vatican City State)','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(26,'Guadeloupe','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(27,'Belize','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(28,'Saint Martin','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(29,'Guernsey','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(30,'Heard Island and McDonald Islands','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(31,'Indonesia','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(32,'Cuba','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(33,'Mongolia','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(34,'Western Sahara','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(35,'Anguilla','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(36,'Korea','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(37,'Serbia','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(38,'Turkmenistan','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(39,'Barbados','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(40,'Trinidad and Tobago','2017-11-12 12:27:48','2017-11-12 12:27:48');

/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table formfields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `formfields`;

CREATE TABLE `formfields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `render_type` enum('textfield','textarea','email','select','select2','checkbox') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'textfield',
  `field_type` enum('json','query','function') COLLATE utf8mb4_unicode_ci DEFAULT 'json',
  `data_value` text COLLATE utf8mb4_unicode_ci,
  `form_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `formfields_title_unique` (`title`),
  UNIQUE KEY `formfields_field_name_unique` (`field_name`),
  KEY `formfields_form_id_foreign` (`form_id`),
  CONSTRAINT `formfields_form_id_foreign` FOREIGN KEY (`form_id`) REFERENCES `forms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `formfields` WRITE;
/*!40000 ALTER TABLE `formfields` DISABLE KEYS */;

INSERT INTO `formfields` (`id`, `title`, `field_name`, `render_type`, `field_type`, `data_value`, `form_id`, `created_at`, `updated_at`)
VALUES
	(1,'fdsqf','product','select2','json','1;prod1\r\n2;prod2',1,'2017-11-12 12:28:08','2017-11-12 12:28:08');

/*!40000 ALTER TABLE `formfields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table forms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `forms`;

CREATE TABLE `forms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_emails` text COLLATE utf8mb4_unicode_ci,
  `mailcontent_sender` text COLLATE utf8mb4_unicode_ci,
  `mailcontent_receiver` text COLLATE utf8mb4_unicode_ci,
  `intro` text COLLATE utf8mb4_unicode_ci,
  `message_ok` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `forms_title_unique` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `forms` WRITE;
/*!40000 ALTER TABLE `forms` DISABLE KEYS */;

INSERT INTO `forms` (`id`, `title`, `subject`, `sender_email`, `sender_name`, `receiver_emails`, `mailcontent_sender`, `mailcontent_receiver`, `intro`, `message_ok`, `created_at`, `updated_at`)
VALUES
	(1,'Form1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(2,'titel',NULL,'email','naam','vanslambrouckd@hotmail.com','mail inh verzender','mail inh ontvanger','intro','message ok','2017-11-30 12:31:41','2017-11-30 12:31:41');

/*!40000 ALTER TABLE `forms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table media
# ------------------------------------------------------------

DROP TABLE IF EXISTS `media`;

CREATE TABLE `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `order_column` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;

INSERT INTO `media` (`id`, `model_id`, `model_type`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `order_column`, `created_at`, `updated_at`)
VALUES
	(1,1,'App\\Product','images','818870','818870.png','image/png','media',37878,X'5B5D',X'7B227469746C65223A2022746974656C222C20226465736372697074696F6E223A20226F6D73636872696A76696E67227D',1,'2017-11-30 12:58:26','2017-11-30 13:16:06'),
	(2,1,'App\\Product','detail','821354','821354.png','image/png','media',27491,X'5B5D',X'5B5D',2,'2017-11-30 12:58:34','2017-11-30 13:12:07'),
	(3,1,'App\\Product','detail','821354 (1)','821354 (1).png','image/png','media',27491,X'5B5D',X'5B5D',1,'2017-11-30 12:58:34','2017-11-30 13:12:07'),
	(4,1,'App\\Product','detail','821354','821354.png','image/png','media',27491,X'5B5D',X'5B5D',3,'2017-11-30 13:11:16','2017-11-30 13:11:21');

/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table menu_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu_items`;

CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_window` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;

INSERT INTO `menu_items` (`id`, `title`, `action`, `url`, `new_window`, `created_at`, `updated_at`)
VALUES
	(1,'fdqsfsdqsf','dsqfdsq','fdqs',0,'2017-11-12 12:30:00','2017-11-23 15:19:26'),
	(2,'\"titel','action','url',1,'2017-11-30 11:56:14','2017-11-30 11:56:46');

/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1856,'2014_10_12_000000_create_users_table',1),
	(1857,'2014_10_12_100000_create_password_resets_table',1),
	(1858,'2017_10_19_142345_create_products_table',1),
	(1859,'2017_10_19_142723_create_categories_table',1),
	(1860,'2017_10_19_163333_create_media_table',1),
	(1861,'2017_10_22_083851_create_brands_table',1),
	(1862,'2017_10_29_094458_CreateReviewsTable',1),
	(1863,'2017_10_29_160444_create_countries_table',1),
	(1864,'2017_11_01_234202_create_menu_items_table',1),
	(1865,'2017_11_02_104616_create_tag_tables',1),
	(1866,'2017_11_02_164208_create_specifications_table',1),
	(1867,'2017_11_09_185819_create_forms_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table product_specifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_specifications`;

CREATE TABLE `product_specifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `specifications_id` int(11) NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_specifications_product_id_specifications_id_unique` (`product_id`,`specifications_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `product_specifications` WRITE;
/*!40000 ALTER TABLE `product_specifications` DISABLE KEYS */;

INSERT INTO `product_specifications` (`id`, `product_id`, `specifications_id`, `value`)
VALUES
	(1,1,1,'fabrikanten\"code'),
	(2,2,1,NULL),
	(3,6,1,NULL),
	(8,1,13,'geh\"eugenkartlezer'),
	(9,2,13,NULL),
	(10,4,13,NULL),
	(11,5,13,NULL),
	(12,1,8,NULL),
	(13,1,9,NULL);

/*!40000 ALTER TABLE `product_specifications` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `price` decimal(6,2) NOT NULL,
  `price_promo` decimal(6,2) DEFAULT NULL,
  `secondhand` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock` int(11) NOT NULL DEFAULT '0',
  `brand_id` int(10) unsigned DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `products_title_unique` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `price`, `price_promo`, `secondhand`, `title`, `stock`, `brand_id`, `description`, `created_at`, `updated_at`)
VALUES
	(1,395.00,10.00,1,'Apple iPhone 6 32GB Grijs',1,6,'Een goede camera en een gebruiksvriendelijk besturingssysteem in een handzame behuizing, dat is Apple iPhone 6. Ten opzichte van zijn voorganger, de iPhone 5s, is het scherm iets gegroeid naar 4,7 inch. Hierdoor kijk je nog prettiger foto\'s en video\'s op je telefoon zonder dat je het toestel niet meer met 1 hand kunt bedienen.<br /><br />\r\n        Standaard meegeleverd:<br /><br />\r\n\r\n        Lightning naar usb kabel<br />\r\n        Earpods headset<br />\r\n        Oplader<br />\r\n        Handleiding',NULL,'2017-11-30 11:30:17'),
	(2,249.00,5.00,0,'SAMSUNG GALAXY J5 (2017) DUAL SIM ZWART',0,6,'De Samsung Galaxy J5 (2017) is een stevig toestel met een stijlvolle uitstraling dankzij de metalen behuizing. Met deze robuuste smartphone maak je scherpe foto\'s. Het toestel heeft zowel aan de voorkant als aan de achterkant een 13 megapixel camera met flitser. Zo leg je je omgeving en jezelf mooi vast, ook bij weinig licht. De telefoon beschikt over een intern geheugen van 16 GB om je foto\'s op te slaan. Dit geheugen is tot 256 GB uit te breiden met een geheugenkaart. Of plaats een tweede simkaart zodat je op 2 telefoonnummers bereikbaar bent. Je ontgrendelt het toestel razendsnel via de vingerafdrukscanner, zodat je gemakkelijk belt, foto\'s maakt of een appje stuurt.<br /><br />\r\n        Standaard meegeleverd:<br /><br />\r\n\r\n        Lightning naar usb kabel<br />\r\n        Earpods headset<br />\r\n        Oplader<br />\r\n        Handleiding',NULL,'2017-11-30 11:30:17'),
	(3,38.00,5.00,0,'dolor',0,1,'Placeat ratione est accusamus velit odio qui. Laborum temporibus veniam eum minus. Corrupti dignissimos ea id reprehenderit. Dolor a error maxime quos odio.','2017-11-12 12:27:48','2017-11-28 19:00:32'),
	(4,55.00,5.00,1,'ut',0,4,'Nam aut reprehenderit eaque ullam. Ex perspiciatis modi sed dicta quisquam rem.','2017-11-12 12:27:48','2017-11-23 15:09:20'),
	(5,65.00,NULL,0,'sint',0,NULL,'Consequatur consequatur quam maiores qui architecto consequatur. Est quia dolorem quam molestiae enim. Enim nemo quas repellat cumque.','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(6,44.00,5.00,0,'sed',0,5,'Velit voluptatem ducimus voluptas optio non. Et provident qui sint aut odit. Minus ex maxime asperiores non rerum quo qui.','2017-11-12 12:27:48','2017-11-26 13:39:45'),
	(7,23.00,NULL,0,'perspiciatis',0,NULL,'Odit quod omnis voluptatem nihil esse. Dolores nostrum quia voluptas blanditiis. At facilis in expedita tempora.','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(8,7.00,NULL,0,'eos',0,NULL,'Facilis similique quidem maiores assumenda sit id. Sit ut impedit autem perspiciatis perspiciatis.','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(9,88.00,NULL,0,'quia',0,NULL,'Quia ad dolorem cum vel atque at quia ut. Architecto reprehenderit tempora expedita quia aut similique. Commodi ipsum explicabo quod dolor voluptas sint at.','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(10,6.00,NULL,0,'dolorem',0,NULL,'Reprehenderit ut iusto aut blanditiis. Iste et libero nostrum. Velit unde doloribus earum consequatur aut. Error aspernatur maxime sequi eum.','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(11,4.00,NULL,0,'modi',0,NULL,'Recusandae omnis ipsum nobis numquam ut illum. Laboriosam eligendi ullam porro aspernatur. Vel tempore et architecto asperiores ipsum. Sed minima qui quae vitae quod nostrum nulla. Voluptate dolores neque blanditiis debitis voluptate minus debitis.','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(12,96.00,NULL,0,'beatae',0,NULL,'Id et officia illo esse qui quod tempora velit. Earum vitae itaque omnis consequatur ut et omnis qui. Voluptate eius voluptatibus est assumenda.','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(13,10.50,9.95,1,'titel',5,1,'omschrijving\r\nregel 2','2017-11-30 11:23:41','2017-11-30 11:23:41'),
	(14,11.25,10.95,1,'fds',0,NULL,'fdsq','2017-11-30 11:27:02','2017-11-30 11:27:02');

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table reviews
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reviews`;

CREATE TABLE `reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reviews_user_id_foreign` (`user_id`),
  CONSTRAINT `reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;

INSERT INTO `reviews` (`id`, `description`, `user_id`, `product_id`, `created_at`, `updated_at`)
VALUES
	(1,'Queen said to herself; \'his eyes are so VERY tired of sitting by her sister kissed her, and said, very gravely, \'I think, you ought to speak, and no one listening, this time, as it was only too glad to find that the Gryphon said, in a low voice, to the other, looking uneasily at the bottom of a well?\' The Dormouse again took a minute or two, she made out that she wanted to send the hedgehog had unrolled itself, and began smoking again. This time there were ten of them, with her head! Off--\'.',52,NULL,'2017-11-12 12:27:48','2017-11-26 13:33:20'),
	(2,'There was a dispute going on within--a constant howling and sneezing, and every now and then; such as, \'Sure, I don\'t put my arm round your waist,\' the Duchess replied, in a VERY good opportunity for croqueting one of these cakes,\' she thought, \'and hand round the court with a growl, And concluded the banquet--] \'What IS the use of a water-well,\' said the King, who had been running half an hour or so, and giving it a violent blow underneath her chin: it had grown in the flurry of the March.',7,1,'2017-11-12 12:27:48','2017-11-30 13:31:11'),
	(3,'Dormouse. \'Don\'t talk nonsense,\' said Alice in a great deal to ME,\' said the Caterpillar. \'Well, I can\'t get out again. That\'s all.\' \'Thank you,\' said the King. \'Nearly two miles high,\' added the Dormouse, after thinking a minute or two, which gave the Pigeon the opportunity of taking it away. She did it so yet,\' said the cook. The King laid his hand upon her face. \'Wake up, Alice dear!\' said her sister; \'Why, what are YOUR shoes done with?\' said the Queen. \'Never!\' said the March Hare meekly.',25,1,'2017-11-12 12:27:48','2017-11-30 13:31:11'),
	(4,'This speech caused a remarkable sensation among the branches, and every now and then another confusion of voices--\'Hold up his head--Brandy now--Don\'t choke him--How was it, old fellow? What happened to me! When I used to it!\' pleaded poor Alice began to tremble. Alice looked at the top of it. Presently the Rabbit say to this: so she went on, looking anxiously about her. \'Oh, do let me hear the name \'W. RABBIT\' engraved upon it. She felt very lonely and low-spirited. In a minute or two, it was.',78,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(6,'How I wonder what you\'re talking about,\' said Alice. \'Why not?\' said the March Hare said--\' \'I didn\'t!\' the March Hare. \'I didn\'t mean it!\' pleaded poor Alice began in a very poor speaker,\' said the King, \'or I\'ll have you got in your knocking,\' the Footman remarked, \'till tomorrow--\' At this the whole party look so grave that she was small enough to drive one crazy!\' The Footman seemed to her lips. \'I know SOMETHING interesting is sure to make personal remarks,\' Alice said nothing: she had.',94,13,'2017-11-12 12:27:48','2017-11-30 11:23:56'),
	(8,'Dodo replied very readily: \'but that\'s because it stays the same thing, you know.\' \'Not at all,\' said the last words out loud, and the moment he was gone, and, by the soldiers, who of course you know about it, even if my head would go round and swam slowly back again, and that\'s very like a steam-engine when she first saw the White Rabbit, \'and that\'s why. Pig!\' She said this last remark. \'Of course you know what \"it\" means well enough, when I breathe\"!\' \'It IS a long silence after this, and.',51,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(9,'Alice. \'Call it what you mean,\' said Alice. The King turned pale, and shut his eyes.--\'Tell her about the crumbs,\' said the Duchess; \'I never could abide figures!\' And with that she was beginning to think about it, even if my head would go anywhere without a cat! It\'s the most curious thing I ask! It\'s always six o\'clock now.\' A bright idea came into her eyes--and still as she was beginning to end,\' said the King, \'that only makes the world you fly, Like a tea-tray in the world am I? Ah.',47,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(10,'Conqueror, whose cause was favoured by the fire, stirring a large mushroom growing near her, she began, rather timidly, saying to herself, \'in my going out altogether, like a frog; and both the hedgehogs were out of sight: then it watched the White Rabbit. She was a good deal: this fireplace is narrow, to be sure; but I don\'t keep the same year for such a wretched height to rest herself, and nibbled a little three-legged table, all made of solid glass; there was no one could possibly hear.',64,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(11,'He says it kills all the time he had come to the end: then stop.\' These were the two creatures got so much surprised, that for two reasons. First, because I\'m on the bank--the birds with draggled feathers, the animals with their hands and feet, to make out what it was perfectly round, she found to be lost: away went Alice like the look of the ground--and I should understand that better,\' Alice said to herself, and fanned herself with one elbow against the roof of the way down one side and then.',57,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(12,'Duchess: \'what a clear way you have to whisper a hint to Time, and round goes the clock in a very difficult game indeed. The players all played at once to eat or drink under the circumstances. There was a bright brass plate with the lobsters, out to sea!\" But the snail replied \"Too far, too far!\" and gave a little glass table. \'Now, I\'ll manage better this time,\' she said these words her foot as far as they came nearer, Alice could not even get her head made her draw back in a trembling.',26,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(13,'Alice. \'Anything you like,\' said the Dodo, pointing to the waving of the doors of the trees upon her arm, that it signifies much,\' she said this, she came suddenly upon an open place, with a smile. There was nothing so VERY wide, but she was ever to get out again. Suddenly she came rather late, and the whole party look so grave that she never knew so much about a whiting before.\' \'I can hardly breathe.\' \'I can\'t go no lower,\' said the Footman, \'and that for two Pennyworth only of beautiful.',93,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(14,'Hatter. He had been wandering, when a sharp hiss made her so savage when they liked, and left off when they liked, so that by the way wherever she wanted much to know, but the Dodo replied very solemnly. Alice was not a moment like a frog; and both the hedgehogs were out of the goldfish kept running in her French lesson-book. The Mouse looked at it gloomily: then he dipped it into one of the what?\' said the Rabbit\'s voice; and the pool rippling to the jury. \'Not yet, not yet!\' the Rabbit.',13,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(15,'I should understand that better,\' Alice said with a knife, it usually bleeds; and she crossed her hands up to her great delight it fitted! Alice opened the door that led into a conversation. \'You don\'t know the song, perhaps?\' \'I\'ve heard something like this:-- \'Fury said to herself, for this time with great curiosity, and this was her turn or not. \'Oh, PLEASE mind what you\'re doing!\' cried Alice, quite forgetting in the court!\' and the Queen was close behind her, listening: so she turned to.',47,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(16,'The Queen\'s argument was, that anything that looked like the Queen?\' said the Duchess; \'I never went to him,\' said Alice sadly. \'Hand it over afterwards, it occurred to her daughter \'Ah, my dear! Let this be a very grave voice, \'until all the other side of WHAT?\' thought Alice; \'but a grin without a cat! It\'s the most interesting, and perhaps as this is May it won\'t be raving mad--at least not so mad as it left no mark on the look-out for serpents night and day! Why, I haven\'t been invited.',68,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(17,'I vote the young man said, \'And your hair has become very white; And yet you incessantly stand on their faces, so that her idea of the wood for fear of killing somebody, so managed to put everything upon Bill! I wouldn\'t say anything about it, even if my head would go through,\' thought poor Alice, and tried to open it; but, as the Dormouse went on, \'and most things twinkled after that--only the March Hare. The Hatter was the Rabbit in a tone of delight, which changed into alarm in another.',10,15,'2017-11-12 12:27:48','2017-11-30 11:28:01'),
	(18,'Alice, surprised at this, but at last in the middle, nursing a baby; the cook had disappeared. \'Never mind!\' said the Gryphon as if it had a consultation about this, and Alice looked up, and there she saw maps and pictures hung upon pegs. She took down a good character, But said I could say if I fell off the fire, and at once and put it right; \'not that it was looking up into the loveliest garden you ever see you any more!\' And here poor Alice began telling them her adventures from the Queen.',52,15,'2017-11-12 12:27:48','2017-11-30 11:28:01'),
	(19,'England the nearer is to do that,\' said the Mock Turtle drew a long hookah, and taking not the right height to rest her chin in salt water. Her first idea was that you weren\'t to talk about wasting IT. It\'s HIM.\' \'I don\'t believe there\'s an atom of meaning in it, and fortunately was just beginning to feel a little shriek and a pair of the way of escape, and wondering what to uglify is, you see, Miss, we\'re doing our best, afore she comes, to--\' At this moment Alice felt a violent blow.',9,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(20,'Footman continued in the middle. Alice kept her waiting!\' Alice felt a violent blow underneath her chin: it had finished this short speech, they all moved off, and Alice was thoroughly puzzled. \'Does the boots and shoes!\' she repeated in a few minutes that she wanted much to know, but the Hatter said, tossing his head mournfully. \'Not I!\' said the Mock Turtle\'s Story \'You can\'t think how glad I am very tired of being all alone here!\' As she said to herself, and began to get her head made her.',78,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(21,'Alice aloud, addressing nobody in particular. \'She\'d soon fetch it back!\' \'And who is Dinah, if I know I do!\' said Alice to herself, as she could. \'The game\'s going on shrinking rapidly: she soon made out what she was holding, and she looked up and throw us, with the Dormouse. \'Write that down,\' the King said to the end: then stop.\' These were the cook, to see if he had to run back into the roof bear?--Mind that loose slate--Oh, it\'s coming down! Heads below!\' (a loud crash)--\'Now, who did.',79,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(22,'HE was.\' \'I never was so large in the other. \'I beg pardon, your Majesty,\' the Hatter continued, \'in this way:-- \"Up above the world am I? Ah, THAT\'S the great wonder is, that there\'s any one of the ground.\' So she was going to begin with,\' said the Cat, as soon as it could go, and broke off a bit of the Nile On every golden scale! \'How cheerfully he seems to like her, down here, and I\'m sure _I_ shan\'t be able! I shall think nothing of tumbling down stairs! How brave they\'ll all think me at.',11,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(23,'Cat went on, \'you see, a dog growls when it\'s angry, and wags its tail about in the kitchen that did not venture to ask help of any one; so, when the race was over. However, when they saw the White Rabbit: it was perfectly round, she came upon a heap of sticks and dry leaves, and the Dormouse say?\' one of the March Hare. Alice sighed wearily. \'I think you can find out the answer to it?\' said the King. The White Rabbit cried out, \'Silence in the last time she went on. \'I do,\' Alice hastily.',32,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(24,'Alice called after her. \'I\'ve something important to say!\' This sounded promising, certainly: Alice turned and came back again. \'Keep your temper,\' said the Gryphon. \'Do you know about it, even if I shall have somebody to talk about her other little children, and everybody else. \'Leave off that!\' screamed the Gryphon. \'Well, I never was so long since she had put on his spectacles. \'Where shall I begin, please your Majesty?\' he asked. \'Begin at the March Hare. Alice sighed wearily. \'I think you.',96,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(25,'White Rabbit, \'and that\'s why. Pig!\' She said the Gryphon. \'They can\'t have anything to say, she simply bowed, and took the hookah out of a feather flock together.\"\' \'Only mustard isn\'t a bird,\' Alice remarked. \'Right, as usual,\' said the White Rabbit with pink eyes ran close by it, and kept doubling itself up very carefully, remarking, \'I really must be shutting up like a thunderstorm. \'A fine day, your Majesty!\' the Duchess said after a minute or two, looking for eggs, I know who I WAS when.',34,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(26,'Allow me to sell you a present of everything I\'ve said as yet.\' \'A cheap sort of way to hear the rattle of the leaves: \'I should like to be managed? I suppose it doesn\'t mind.\' The table was a bright idea came into Alice\'s shoulder as she swam nearer to make out which were the two creatures got so close to her in such a curious dream, dear, certainly: but now run in to your places!\' shouted the Queen had ordered. They very soon finished off the cake. * * * * * * * * * * * * * * * * * * * * * *.',75,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(27,'Duchess: you\'d better ask HER about it.\' (The jury all brightened up again.) \'Please your Majesty,\' the Hatter continued, \'in this way:-- \"Up above the world she was talking. Alice could hear the Rabbit noticed Alice, as she ran. \'How surprised he\'ll be when he sneezes; For he can thoroughly enjoy The pepper when he finds out who was a little wider. \'Come, it\'s pleased so far,\' thought Alice, as she could. The next witness was the BEST butter, you know.\' \'And what an ignorant little girl or a.',48,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(31,'Alice dodged behind a great hurry. An enormous puppy was looking at everything about her, to pass away the moment they saw her, they hurried back to the general conclusion, that wherever you go to law: I will prosecute YOU.--Come, I\'ll take no denial; We must have been ill.\' \'So they were,\' said the Mock Turtle drew a long way. So she set the little door, so she bore it as to prevent its undoing itself,) she carried it off. \'If everybody minded their own business,\' the Duchess by this time.).',18,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(32,'Queen said severely \'Who is this?\' She said this last remark. \'Of course not,\' said the Dormouse, who seemed to think that there was silence for some way of settling all difficulties, great or small. \'Off with his head!\' or \'Off with her head!\' Those whom she sentenced were taken into custody by the whole thing very absurd, but they began solemnly dancing round and round goes the clock in a deep voice, \'are done with a shiver. \'I beg your acceptance of this elegant thimble\'; and, when it saw.',45,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(33,'I get\" is the same words as before, \'and things are \"much of a well?\' \'Take some more of the legs of the tea--\' \'The twinkling of the jurors had a vague sort of mixed flavour of cherry-tart, custard, pine-apple, roast turkey, toffee, and hot buttered toast,) she very seldom followed it), and sometimes shorter, until she made some tarts, All on a bough of a water-well,\' said the Cat, \'a dog\'s not mad. You grant that?\' \'I suppose so,\' said the Gryphon, and all sorts of little pebbles came.',90,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(34,'So she stood still where she was, and waited. When the sands are all pardoned.\' \'Come, THAT\'S a good deal until she made her look up and bawled out, \"He\'s murdering the time! Off with his knuckles. It was all ridges and furrows; the balls were live hedgehogs, the mallets live flamingoes, and the words \'DRINK ME\' beautifully printed on it except a little pattering of feet in the pool, \'and she sits purring so nicely by the prisoner to--to somebody.\' \'It must be on the same when I get it home?\'.',68,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(35,'I should think it so quickly that the way the people near the door of which was full of tears, but said nothing. \'Perhaps it doesn\'t matter much,\' thought Alice, \'shall I NEVER get any older than I am now? That\'ll be a very difficult question. However, at last she stretched her arms folded, quietly smoking a long breath, and said nothing. \'Perhaps it doesn\'t mind.\' The table was a little scream of laughter. \'Oh, hush!\' the Rabbit say, \'A barrowful will do, to begin with.\' \'A barrowful will do.',60,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(36,'Duchess replied, in a great hurry, muttering to itself in a VERY turn-up nose, much more like a steam-engine when she caught it, and yet it was a queer-shaped little creature, and held out its arms and frowning at the mushroom (she had grown in the court!\' and the constant heavy sobbing of the guinea-pigs cheered, and was delighted to find her in such a dear quiet thing,\' Alice went timidly up to the Caterpillar, and the Panther received knife and fork with a sigh. \'I only took the hookah out.',6,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(37,'So she began: \'O Mouse, do you know the meaning of it at last, more calmly, though still sobbing a little girl,\' said Alice, in a trembling voice to a day-school, too,\' said Alice; \'it\'s laid for a minute, trying to find any. And yet you incessantly stand on their slates, when the White Rabbit. She was walking hand in hand with Dinah, and saying to herself, as usual. I wonder if I shall have some fun now!\' thought Alice. \'Now we shall have somebody to talk to.\' \'How are you getting on now, my.',69,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(38,'March Hare went \'Sh! sh!\' and the whole thing, and longed to get in at the Footman\'s head: it just missed her. Alice caught the flamingo and brought it back, the fight was over, and both the hedgehogs were out of his shrill little voice, the name of nearly everything there. \'That\'s the judge,\' she said this, she noticed that they couldn\'t see it?\' So she set off at once: one old Magpie began wrapping itself up very sulkily and crossed over to herself, \'whenever I eat one of the Rabbit\'s little.',50,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(39,'CHAPTER VIII. The Queen\'s argument was, that her shoulders were nowhere to be no chance of this, so that they could not make out at the end of every line: \'Speak roughly to your little boy, And beat him when he sneezes; For he can thoroughly enjoy The pepper when he pleases!\' CHORUS. \'Wow! wow! wow!\' While the Owl and the jury asked. \'That I can\'t put it more clearly,\' Alice replied very politely, \'if I had it written down: but I THINK I can guess that,\' she added aloud. \'Do you take me for.',78,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(40,'I was a most extraordinary noise going on within--a constant howling and sneezing, and every now and then another confusion of voices--\'Hold up his head--Brandy now--Don\'t choke him--How was it, old fellow? What happened to me! I\'LL soon make you dry enough!\' They all returned from him to be talking in his turn; and both footmen, Alice noticed, had powdered hair that curled all over crumbs.\' \'You\'re wrong about the temper of your flamingo. Shall I try the patience of an oyster!\' \'I wish I.',57,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(41,'Alice quite jumped; but she saw in my kitchen AT ALL. Soup does very well to say \'I once tasted--\' but checked herself hastily. \'I thought you did,\' said the White Rabbit read out, at the proposal. \'Then the Dormouse shook its head impatiently, and walked a little shriek, and went stamping about, and make THEIR eyes bright and eager with many a strange tale, perhaps even with the Duchess, as she could have told you butter wouldn\'t suit the works!\' he added looking angrily at the top of her own.',93,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(42,'Duchess was VERY ugly; and secondly, because they\'re making such VERY short remarks, and she grew no larger: still it was looking about for some way of nursing it, (which was to twist it up into the jury-box, or they would call after her: the last few minutes, and began to repeat it, but her head through the neighbouring pool--she could hear him sighing as if a fish came to ME, and told me you had been to a farmer, you know, and he checked himself suddenly: the others looked round also, and.',65,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(43,'Mock Turtle: \'why, if a fish came to ME, and told me he was going to be, from one minute to another! However, I\'ve got to the table for it, you know--\' She had quite forgotten the words.\' So they sat down, and nobody spoke for some time busily writing in his confusion he bit a large one, but the Mouse to tell me the truth: did you manage on the floor, as it settled down again, the Dodo could not taste theirs, and the pattern on their slates, when the White Rabbit blew three blasts on the.',26,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(44,'Mouse heard this, it turned a back-somersault in at the March Hare. Alice was thoroughly puzzled. \'Does the boots and shoes!\' she repeated in a confused way, \'Prizes! Prizes!\' Alice had not noticed before, and she heard was a table in the window, and on it except a little shaking among the bright eager eyes were nearly out of the court. All this time the Mouse to Alice for protection. \'You shan\'t be able! I shall have some fun now!\' thought Alice. \'I don\'t know much,\' said Alice, (she had kept.',9,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(45,'I\'ve finished.\' So they sat down, and felt quite strange at first; but she did not at all anxious to have no idea how confusing it is all the time they had settled down again, the Dodo said, \'EVERYBODY has won, and all the rest, Between yourself and me.\' \'That\'s the most curious thing I ask! It\'s always six o\'clock now.\' A bright idea came into Alice\'s shoulder as he could go. Alice took up the fan she was as long as it went. So she began: \'O Mouse, do you want to stay with it as far down the.',100,NULL,'2017-11-12 12:27:48','2017-11-26 18:38:52'),
	(46,'WAS no one could possibly hear you.\' And certainly there was no one to listen to her, And mentioned me to introduce it.\' \'I don\'t even know what you mean,\' said Alice. \'Oh, don\'t bother ME,\' said Alice desperately: \'he\'s perfectly idiotic!\' And she began looking at the sudden change, but very politely: \'Did you say pig, or fig?\' said the Duchess, \'chop off her head!\' the Queen jumped up and saying, \'Thank you, it\'s a very poor speaker,\' said the Hatter. \'You might just as if it likes.\' \'I\'d.',10,NULL,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(48,'I\'m somebody else\"--but, oh dear!\' cried Alice, jumping up and down looking for the baby, it was impossible to say \'Drink me,\' but the Dodo managed it.) First it marked out a new pair of the game, feeling very glad to find herself talking familiarly with them, as if nothing had happened. \'How am I to get in?\' \'There might be hungry, in which the cook had disappeared. \'Never mind!\' said the King. \'Nearly two miles high,\' added the Hatter, \'you wouldn\'t talk about cats or dogs either, if you.',86,14,'2017-11-12 12:27:48','2017-11-30 11:27:02'),
	(49,'Pigeon; \'but if they do, why then they\'re a kind of thing never happened, and now here I am very tired of this. I vote the young man said, \'And your hair has become very white; And yet you incessantly stand on your head-- Do you think, at your age, it is you hate--C and D,\' she added aloud. \'Do you know the song, \'I\'d have said to herself, and nibbled a little bird as soon as the March Hare. Alice was not a moment that it signifies much,\' she said to the Queen. \'Never!\' said the Hatter, \'or.',47,2,'2017-11-12 12:27:48','2017-11-30 11:51:50'),
	(50,'Alice hastily; \'but I\'m not Ada,\' she said, \'and see whether it\'s marked \"poison\" or not\'; for she was surprised to see you any more!\' And here poor Alice in a deep voice, \'are done with blacking, I believe.\' \'Boots and shoes under the sea,\' the Gryphon at the March Hare. Alice was too much frightened that she wasn\'t a bit hurt, and she felt sure she would gather about her any more questions about it, even if I must, I must,\' the King triumphantly, pointing to the little crocodile Improve his.',52,5,'2017-11-12 12:27:48','2017-11-26 13:21:13'),
	(51,'omschrijving',81,2,'2017-11-30 11:38:03','2017-11-30 11:38:03');

/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table specifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `specifications`;

CREATE TABLE `specifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `specification` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `specifications` WRITE;
/*!40000 ALTER TABLE `specifications` DISABLE KEYS */;

INSERT INTO `specifications` (`id`, `specification`, `created_at`, `updated_at`)
VALUES
	(1,'Fabrikantcode','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(2,'Artikelnummer','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(3,'Garantie','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(4,'Garantietype','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(5,'Gewicht','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(6,'Hoogte','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(7,'Breedte','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(8,'Versie IOS','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(9,'Besturingssysteem','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(10,'Introductiedatum','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(11,'Geheugenkaartlezer','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(12,'Opslagcapaciteit','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(13,'Geheugenkaartlezer','2017-11-12 12:27:48','2017-11-12 12:27:48');

/*!40000 ALTER TABLE `specifications` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table taggables
# ------------------------------------------------------------

DROP TABLE IF EXISTS `taggables`;

CREATE TABLE `taggables` (
  `tag_id` int(10) unsigned NOT NULL,
  `taggable_id` int(10) unsigned NOT NULL,
  `taggable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  KEY `taggables_tag_id_foreign` (`tag_id`),
  CONSTRAINT `taggables_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `taggables` WRITE;
/*!40000 ALTER TABLE `taggables` DISABLE KEYS */;

INSERT INTO `taggables` (`tag_id`, `taggable_id`, `taggable_type`)
VALUES
	(1,1,'App\\Specifications'),
	(1,2,'App\\Specifications'),
	(1,3,'App\\Specifications'),
	(1,4,'App\\Specifications'),
	(2,5,'App\\Specifications'),
	(2,6,'App\\Specifications'),
	(2,7,'App\\Specifications'),
	(3,8,'App\\Specifications'),
	(3,9,'App\\Specifications'),
	(3,10,'App\\Specifications'),
	(4,11,'App\\Specifications'),
	(5,12,'App\\Specifications'),
	(4,13,'App\\Specifications'),
	(6,2,'App\\Product'),
	(6,1,'App\\Product'),
	(7,1,'App\\Product'),
	(8,1,'App\\Product'),
	(9,1,'App\\Product'),
	(8,3,'App\\Product'),
	(10,3,'App\\Product'),
	(7,13,'App\\Product'),
	(8,13,'App\\Product'),
	(11,13,'App\\Product');

/*!40000 ALTER TABLE `taggables` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` json NOT NULL,
  `slug` json NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_column` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;

INSERT INTO `tags` (`id`, `name`, `slug`, `type`, `order_column`, `created_at`, `updated_at`)
VALUES
	(1,X'7B22656E223A202250726F64756374227D',X'7B22656E223A202270726F64756374227D','specifications',1,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(2,X'7B22656E223A202246797369656B6520656967656E736368617070656E227D',X'7B22656E223A202266797369656B652D656967656E736368617070656E227D','specifications',2,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(3,X'7B22656E223A2022416C67656D656E6520656967656E736368617070656E227D',X'7B22656E223A2022616C67656D656E652D656967656E736368617070656E227D','specifications',3,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(4,X'7B22656E223A2022476568657567656E227D',X'7B22656E223A2022676568657567656E227D','specifications',4,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(5,X'7B22656E223A202252414D2D476568657567656E227D',X'7B22656E223A202272616D2D676568657567656E227D','specifications',5,'2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(6,X'7B22656E223A2022656E7720746167227D',X'7B22656E223A2022656E772D746167227D','products',6,'2017-11-23 14:42:48','2017-11-23 14:42:48'),
	(7,X'7B22656E223A20226E6577207461672032227D',X'7B22656E223A20226E65772D7461672D32227D','products',7,'2017-11-26 19:05:14','2017-11-26 19:05:14'),
	(8,X'7B22656E223A2022746573743939227D',X'7B22656E223A2022746573743939227D','products',8,'2017-11-26 19:08:23','2017-11-26 19:08:23'),
	(9,X'7B22656E223A20226664736664737166647371227D',X'7B22656E223A20226664736664737166647371227D','products',9,'2017-11-26 19:32:32','2017-11-26 19:32:32'),
	(10,X'7B22656E223A202274616734227D',X'7B22656E223A202274616734227D',NULL,10,'2017-11-28 19:01:43','2017-11-28 19:01:43'),
	(11,X'7B22656E223A20227465737431323233227D',X'7B22656E223A20227465737431323233227D','products',11,'2017-11-30 11:23:56','2017-11-30 11:23:56'),
	(12,X'7B22656E223A20227465737431323237227D',X'7B22656E223A20227465737431323237227D','products',12,'2017-11-30 11:28:01','2017-11-30 11:28:01'),
	(14,X'7B22656E223A202274657374207461672031333030227D',X'7B22656E223A2022746573742D7461672D31333030227D','products',14,'2017-11-30 12:01:16','2017-11-30 12:01:16');

/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_country_id_foreign` (`country_id`),
  CONSTRAINT `users_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `country_id`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(2,'Jaydon Morissette III','roslyn78@example.net','UB7kr!2.H<&P8&\\U',NULL,'DQDOFST1db','2017-11-12 12:27:48','2017-11-30 11:53:35'),
	(4,'Dr. Waino Haag DVM','bergstrom.duane@example.com','Pk_v`^~Cp',11,'yA0e71VbaT','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(5,'Kamille Thiel','michale.lehner@example.com','z#^Tm.BKl<><y+a)X',15,'ZfjiQozXSF','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(6,'Mohamed Bahringer II','hettinger.marie@example.net','qM?>Ah`v2e]yI',13,'H4M96SEBrm','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(7,'Mr. Ali Ryan Sr.','voreilly@example.org','`Np8!}Y,#tr5{',2,'ti8nPae5Lq','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(8,'Mrs. Jany Moen','rowe.christa@example.com','giUg&Tus(ka3$KEN^|9H',31,'T7Vrrm5Q2Z','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(9,'Margaret Smitham','abigayle86@example.org',':<bF7nk:P8tHvgV}q1/j',40,'VZX1R6Amp0','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(10,'Kaylie Schmeler','jeremy99@example.org','<AJR\"RBPsx:P0',21,'16K7tyOeL2','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(11,'Prof. Asa Goldner IV','zcrooks@example.net','Xve;/l~vnIh^{fxDvX8<',3,'JS4QtLBpDA','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(12,'Aurelie Corkery','titus.bergstrom@example.com','W\'RU~gOtK`8`S+i\"om~',14,'pZsFCoE04Y','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(13,'Marc Kuhlman PhD','genevieve28@example.org','z94A(7',12,'KANivqzpCD','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(14,'Viviane Lesch','bhilpert@example.org','d1Gv@R!',10,'caLJWPxnYT','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(15,'Eliza Paucek','pfannerstill.zula@example.net','BWdLyW?E>8RGqx&cW7',11,'RPnmYcTDH8','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(16,'Maybell Breitenberg','vstehr@example.org','/W/tDDgCu',40,'CIdrZROsYw','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(17,'Marcelina Zemlak DVM','baumbach.rodrick@example.net','-uPK_#',14,'usYT2FkkbB','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(18,'Myron Lynch','evandervort@example.com','[DPsG_-Q3-k{heDl}<.',11,'pSlTY2HAZY','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(19,'Molly Bartell','pedro.quitzon@example.com',',t3dwbh_45s9Yz',13,'wXNpkXk9xZ','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(20,'Mathew Marquardt III','eloisa34@example.org','SmOT@9vh`xEmT8K',16,'d0n5ELgo7q','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(21,'Retta Welch','alexandra16@example.net','CN_B+Wap].f_KM',17,'K0QUkXIKCl','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(22,'Clifton Emard','qconn@example.org','Ni_#C]oMt1-dHd]s',22,'NhvIZmGmLG','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(23,'Javon Denesik','cordelia.macejkovic@example.net','A8>gH4<:6',12,'fprUceJ7aL','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(24,'Verona Moore','trey.halvorson@example.org','cVJ{u=03}]U(m?',33,'8Mn3ucExsM','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(25,'Vivian Pfannerstill','mohr.lavina@example.org','!XjDP%upwzh',40,'NWKmvfKPrB','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(26,'Gabriella Hermann DDS','glover.breanne@example.com','P<d%[zKx98<',22,'fBCslrnuzA','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(27,'Mrs. Bert Corkery','claudie62@example.com','~M.$)%;on$BG',35,'Pc0ZR1sWcG','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(28,'Luciano Bode','elaina.hettinger@example.org','R72(X0',37,'iBZZKk7AIb','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(29,'Dr. Eugene Dicki PhD','morar.benton@example.net','?aD3{i[HITdF2',36,'e2oTeU5kmZ','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(30,'Lawson Oberbrunner','aufderhar.eugenia@example.org',';Mt6)$U9Zd60*e+2n!',36,'zeTKw2YWOt','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(31,'Mr. Henderson Satterfield','reichel.jovani@example.org','VMzbkC%&_,C:C%',35,'1gEYkpKieQ','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(32,'Mr. Lane Harris Sr.','nicolette34@example.com','Mn]C1ND',14,'BuWBubROgS','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(33,'Prof. Deontae Torphy I','isaiah.kessler@example.com','Oe2]S6W54)\\<pfk$y;',14,'wpRawv608Y','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(34,'Marilyne Sipes','angel99@example.net','r\\8>\\]g1xiH-',31,'TKdcEML3co','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(35,'Ms. Carolanne Kling V','connelly.kaya@example.net','2|F^<taeA\\L5.0rEsFyk',25,'mXg7nZo9ui','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(36,'Prof. Anahi Bernhard','xheathcote@example.com','8zWcf9/n&jv',40,'94fG84Engk','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(37,'Mrs. Glenda Ryan','green.jazmyne@example.net','JmbDX4@',20,'wGbk5IurCk','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(38,'Darian Yost','lwaters@example.net','NK{t{GMDga]',12,'CflBCI7VQj','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(39,'Dr. Dorthy Cruickshank','ignatius.dooley@example.com','[9:}0.Lh5!C~*Fp',31,'P9VF5ivqS7','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(40,'Prof. Lia Hills','ekoelpin@example.org','YLLTW2@tMb]',5,'IZFPGcuh0l','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(41,'Randy Welch','wehner.stanley@example.com','6c-l.8s,Bu;-l:',31,'XtIiCo6A2y','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(42,'Beth Frami Sr.','nkuvalis@example.org','fbK152',35,'917YLzy8uw','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(43,'Josephine Olson','rachel.schmidt@example.net','AdNj:J\"VwN3\'c',22,'0A4T8NgR33','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(44,'Josianne Hermiston','stephen12@example.net','l3A\'|.^/',11,'eOY4ys1yIj','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(45,'Dr. Clara Pouros Sr.','audra23@example.com','5\"fxH0R)BOK{@>9',38,'TqCPLHUarF','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(46,'General Bogisich V','crooks.don@example.net','U#\"0t@I]H*\"u5OrCe',2,'HDpUGlylJJ','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(47,'Jaeden Block','joannie41@example.net','>/\"5x;Lvy,|rSn_S+5',10,'917JPAy9WI','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(48,'Dr. Adeline Kilback','meggie.tillman@example.org','u|.Edz',11,'SzGZL7z88H','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(49,'Claud Bernhard III','vbode@example.net','Lz-,hY',24,'0J62Fu8J7U','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(50,'Dr. Curtis Renner Sr.','mckenzie49@example.com',':Tq#_\"|5*gNp',37,'QAvrDAdgYj','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(51,'Cyril Hartmann','estamm@example.org','$d2gv?NHnmrA_9mV',33,'8r6nloPiIv','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(52,'Marcia Harvey MD','seamus.thompson@example.net','.R3{Y#Y+g',40,'V59cKsyO8L','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(53,'Merritt Lesch V','vicenta85@example.com','EAnbt~R&L',17,'sJavuYpGrl','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(54,'Alford Hamill','conroy.asha@example.net','2UT4>[?GK1',3,'u7RmZu3cwl','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(55,'Mr. Julien Rutherford II','balistreri.queen@example.net',')I6E^]@|',7,'ewd3kfzifQ','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(56,'Brandon Boehm','wrobel@example.org','K9W\'MK)d3!',7,'e2pviQ4Ldx','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(57,'Viviane Kulas','alfonzo.shields@example.com','pT-r|@m~1Z',23,'6wgWMNoKG4','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(58,'Lenore McCullough','mariana21@example.com','S3)`~@1a;0NxNhR;',37,'9en77DMkub','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(59,'Genoveva Barrows','mschneider@example.org','MJNnXb]ZIU',14,'Sc344WTdUP','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(60,'Cicero Gorczany','dimitri04@example.com','YjHb2Ir(=t*6-|2Us;',33,'99HlxHxMNW','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(61,'Alejandrin Luettgen','feil.jensen@example.com','etz/NdN)psCA=g@$d',19,'OVL4UwuDVC','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(62,'Rodger Corwin','susana24@example.com','9=>V#]E4qt#<',32,'XR6YYIAaSH','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(63,'Rod Kling','wilderman.clementina@example.com','E!\'2%B?5%9t.GdbeBd#',30,'8szU3XRiXv','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(64,'Itzel Smith','hermiston.vickie@example.net','ELq?MW_Z:B/jc;;@67',2,'GlUuB3MdoK','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(65,'Hyman Runolfsson PhD','marco01@example.org','d1r.W}',37,'SgBBwAwKRM','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(66,'Genesis Funk','kathryn71@example.net','Yw((iP)%',8,'S6NbMQBuSG','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(67,'Miss Kaylie Kling V','bogan.mylene@example.net','+=?@$b][@+`',18,'Pwccdo31mY','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(68,'Furman Gerlach','lonzo59@example.org','E\'6yUo\"!:z}I@4yWL)',16,'4SWcNq8I8H','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(69,'Prof. Mackenzie Langosh V','woodrow21@example.org','W>S;%:H>?tS^)',6,'vHc7QZDKBc','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(70,'Prof. Jany Corkery II','jules52@example.net','tRY`)2wEO)SXqwpr',26,'XgkCgkEgjr','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(71,'Mathilde Crooks','irwin10@example.com','kmUh4TN#=$wIQru',10,'pognO0Udb6','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(72,'Estelle Goyette DDS','bharris@example.org','e~_\'6:8fl`x',30,'1Z1OwUh0Ke','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(73,'Ellen Langworth','streich.martina@example.org','03ZN}`',36,'m2lmkR0l10','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(74,'Miss Madie Green','hokon@example.com','Os:OHY9@b%r5~7{GKL`K',29,'AVxMavIj89','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(75,'Haley McCullough','bridgette.ledner@example.com','f:~q],',32,'c5LC093cpM','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(76,'Ms. Vita Wilderman','balistreri.elizabeth@example.org','_tjaV-VE95I]&.-6Lic3',35,'ddqrAA4D8j','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(77,'Leta Bayer Jr.','waelchi.evie@example.org','^i[_8fK\\Ol16>',18,'MMXvNMhsyw','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(78,'Amina Jenkins','jimmy.hartmann@example.net','2j%L,tQZ!)ep#',3,'YIDrwQbQX4','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(79,'Prof. Vincenza Fritsch','jfritsch@example.org','<.)H\\6_;LNt\'0iBQY',24,'VFG2xta3fR','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(80,'Gia Bogan','emmerich.bennett@example.net','$x0)O1DQ.U',37,'rXT0GZZ1NO','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(81,'Dr. Gregorio Quitzon DDS','heather06@example.com','\\S21w,=',26,'c7AV1obhQj','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(82,'Miss Hortense Leannon','vhyatt@example.org','G~a#fax1J)03aU',3,'EMk03h4vYW','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(83,'Harry Murray II','gbaumbach@example.com','ooZOp~5g',10,'zMmHpgPt2y','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(84,'Miss Dawn Lueilwitz I','shany.rath@example.com','n869)Mn$o2IQ})',19,'uYqzDdbm3R','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(85,'Susana Schneider','wilhelm00@example.com','>=?o7CjH3xq?<isR',3,'9jruZ8tqyy','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(86,'Kenneth Rolfson','fred11@example.org','X\"6QLO{>H-Wnq$m!)S',12,'08rWkEkA32','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(87,'Coleman Bartoletti DDS','else.rohan@example.com','s(PE7$@G3gQr)j',31,'7onSvhLuWN','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(88,'Dr. Tamara Ullrich I','upton.janae@example.com','9>RbcI^x?wzC',39,'yalj2JQPtO','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(89,'Dr. Jude Hilpert','ida.dooley@example.org','B@.e-)KW7%*&LaG+t',28,'Rrj8n8222j','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(90,'Dr. Lester Mueller','natalia19@example.org','clDTsCZ$',9,'s0wwpA6vS6','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(91,'Mittie Borer','maggio.jeremy@example.net','hsezGO4\\()aYUn$CCkI\"',26,'DZaabLqEtn','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(92,'Lonnie Monahan','greenholt.keegan@example.com','z>gq-4ruhy`%XOf1]CyY',24,'IZY5kZr5aD','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(93,'Johan Ledner','udare@example.net','?7EN>>)Y\\&EF',21,'TcRgr4wiTz','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(94,'Mr. Cloyd Runolfsdottir','rkertzmann@example.net','Te~3tD46\\_u0B-C2\'<H9',37,'lw0kIHixnt','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(95,'Dr. Theresa Deckow DDS','stehr.luis@example.net','%@/\\%9',5,'PbiUfEGdLA','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(96,'Dedrick Sawayn','lehner.evan@example.net','P$|W&G0uqF',25,'zwU21ptrfI','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(97,'Aida Jast','shawna.heathcote@example.net','AkV[R<OW:d|or<{O`7T',21,'HjlKN85Z6l','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(98,'William Gottlieb','felicity37@example.com','/hJ25(g?j704}0r$I',15,'BG2O69UvTL','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(99,'Prof. Constantin Eichmann','maximus.ortiz@example.net','ZNQWlV*\'',34,'nuTPQcR1Mf','2017-11-12 12:27:48','2017-11-12 12:27:48'),
	(100,'Grover Purdy','mariano32@example.net','DyzJT%Y_X)A$S42On',11,'xYrHL4o2YG','2017-11-12 12:27:48','2017-11-12 12:27:48');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
