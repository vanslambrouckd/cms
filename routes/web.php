<?php
use Illuminate\Foundation\Application;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/admin/content/products');
Route::redirect('/admin', '/admin/content/products');

// Route::get('/category', 'CategoryController@index');
// Route::get('/product', 'ProductController@index');

// Route::bind('contentType', function($type) {
//     dd('jaa');
//     return new App\ContentType($type);
// });

// Route::bind('ContentRequest', function($type) {    
//     die('soms');
//     dd($type);
//     dd('jaa');
// });

// Route::bind('type', function($type, $id) {    
//     return App::make('App\Http\Requests\ContentRequest', ['type' => $type]);
// });


Route::prefix('admin')->group(function () {
    Route::resource('categories', 'CategoryController');
    Route::resource('products', 'ProductController');
    Route::resource('brands', 'BrandController');

    Route::get('content/{type}', 'ContentController@index')->name('content.index');

    Route::get('content/{type}/search/remove/{field}', 'ContentController@search_remove_field')->name('content.search_remove_field');
    Route::get('content/{type}/search/clear_all', 'ContentController@search_clear_all')->name('content.search_clear_all');

    Route::post('content/{type}/list', 'ContentController@filter')->name('content.search');
    
    Route::post('content/{type}/store', 'ContentController@store')->name('content.store');
    
    Route::get('content/{type}/create', 'ContentController@create')->name('content.create');

    Route::get('content/{type}/{content}/edit', 'ContentController@edit')->name('content.edit');
    Route::patch('content/{type}/{content}/update', 'ContentController@update')->name('content.update');
    Route::delete('content/{type}/{content}/delete', 'ContentController@destroy')->name('content.delete');

    //sortorder
    Route::post('content/{type}/{content}/{direction}', 'ContentController@indexSort')->name('content.index.sortorder');


    //relations belongstomany
    Route::get('content/{type}/{content}/relations/belongstomany/{belongsToMany}', 'ContentController@relations_belongstomany')->name('content.belongstomany');
    Route::post('content/{type}/{content}/relations/belongstomany/{belongsToMany}/store', 'ContentController@relations_belongstomany_store')->name('content.belongstomany.store');

    //relations belongstomany per tag
    Route::get('content/{type}/{content}/relations/belongstomany_by_tag/{belongsToMany}', 'ContentController@relations_belongstomany_by_tag')->name('content.belongstomany_by_tag');

    //relations hasmany
    Route::get('content/{type}/{content}/relations/hasmany/{hasMany}', 'ContentController@relations_hasmany')->name('content.hasmany');
    Route::post('content/{type}/{content}/relations/hasmany/{hasMany}/store', 'ContentController@relations_hasmany_store')->name('content.hasmany.store');

    //collection
    Route::get('content/{type}/{content}/media/{collection_name}', 'ContentController@mediaIndex')->name('content.media.index');
    Route::delete('content/{type}/{content}/media_delete/{collection_name}/{media_id}', 'ContentController@mediaDestroy')->name('content.media.destroy');
    Route::post('content/{type}/{content}/media_store/{collection_name}', 'ContentController@mediaStore')->name('content.media.store');

    //media metadata
    Route::get('content/{type}/{content}/media_meta_edit/{collection_name}/{media_id}', 'ContentController@mediaMetaEdit')->name('content.media.metaedit');
    Route::patch('content/{type}/{content}/media_meta_store/{collection_name}/{media_id}', 'ContentController@mediaMetaUpdate')->name('content.media.metaupdate');

    //media sortorder
    Route::post('content/{type}/{content}/media_meta_sortorder/{collection_name}/{media_id}/{direction}', 'ContentController@mediaSort')->name('content.media.sort');

     //forms
    Route::get('forms/{form}/preview', 'FormController@preview')->name('content.forms.preview');
    Route::get('forms/{form}/fields', 'FormController@fieldsIndex')->name('content.forms.fields.index');
    Route::get('forms/{form}/fields/create', 'FormController@fieldsCreate')->name('content.forms.fields.create');
    Route::get('forms/{form}/fields/{field}/edit', 'FormController@fieldsEdit')->name('content.forms.fields.edit');
    Route::post('forms/{form}/fields/store', 'FormController@fieldsStore')->name('content.forms.fields.store');
    Route::patch('forms/{form}/fields/{field}/update', 'FormController@fieldsUpdate')->name('content.forms.fields.update');
    Route::delete('forms/{form}/fields/{field}/destroy', 'FormController@fieldsDestroy')->name('content.forms.fields.delete');

    //builder
    Route::get('builder/index', 'CmsBuildController@index')->name('builder.index');
    Route::get('builder/{filename}/edit', 'CmsBuildController@edit')->name('builder.edit');
    Route::post('builder/{filename}/store', 'CmsBuildController@store')->name('builder.store');

    Route::get('builder/get_object_methods', 'CmsBuildController@get_object_methods')->name('builder.get_object_methods');

    //custom voor deze website
    Route::get('products/{product}/specifications', 'ProductController@specificationsIndex')->name('products.specifications');
    Route::post('products/{product}/specifications/store', 'ProductController@specifications_store')->name('products.specifications.store');
});

