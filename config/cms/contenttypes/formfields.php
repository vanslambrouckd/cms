<?php
return [
    'name' => 'Formfield',
    //'table' => 'brands',
    'model' => 'Formfield',
    'model_namespace' => "App\\",
    'pk' => 'id',
    'default_sortfield' => 'title',
    'default_sortorder' => 'asc',
    'fields' => [
        [
        'name' => 'Form id',
        'field_name' => 'form_id',
        'render_type' => 'hidden',
        ],
        [
        'name' => 'Id',
        'field_name' => 'id',
        'heading' => 1,
        ],
        [
        'name' => 'titel',
        'field_name' => 'title',
        'heading' => 1,
        'rules' => 'required|unique:forms,title,$id',
        ],
        [
        'name' => 'Field name',
        'field_name' => 'field_name',
        'heading' => 1,
        'rules' => 'required|unique:forms,field_name,$id',
        ],
        [
        'name' => 'Type',
        'field_name' => 'render_type',
        'render_type' => 'select2',
        'field_type' => 'json',
        'data_value' => 'textfield;textfield
            textarea;textarea
            select;select
            select2;select2
            checkbox;checkbox
            email;email'
        ,
        'heading' => 1,    
        'rules' => 'required',    
        ],        
        [
        'name' => 'Data Type',
        'field_name' => 'data_type',
        'render_type' => 'select2',
        'field_type' => 'json',
        'data_value' => 'json;json
            query;query
            function;function'
        ,
        'heading' => 1,    
        'rules' => 'required',    
        ],        
        [
        'name' => 'Data',
        'field_name' => 'data_value',
        'render_type' => 'textarea',
        'rules' => 'required',
        ],
    ],
    'tabs' => [
    ]
];