<?php

return [
    'name' => 'Brands',
    //'table' => 'brands',
    'model' => 'Brand',
    'model_namespace' => "App\\",
    'pk' => 'id',
    'default_sortfield' => 'title',
    'default_sortorder' => 'asc',
    'fields' => [
        [
        'name' => 'Id',
        'field_name' => 'id',
        'heading' => 1,
        ],
        [
        'name' => 'titel',
        'field_name' => 'title',
        'heading' => 1,
        'rules' => 'required|unique:brands,title,$id',
        'searchable' => 1,
        ],        
    ],
    'tabs' => [
        [
        'name' => 'product', //tab title
        'field_name' => 'products', //data class field_name waarmee je de laravel relatie maakte (return $this->hasMany())
        'field_type' => 'hasMany',
        'render_type' => 'checkbox',
        'related_class' => 'App\Product',
        'related_field' => 'brand_id', //veld waarmee de hasmany opgeslagen wordt
        'data_class' => 'App\Product', //class die de functie bevat waarmee je de lijst ophaalt
        'data_function' => 'all', // functie waarmee je de lijst ophaalt
        'data_value' => 'id', //lijst item id
        'data_label' => 'title', //lijst item value
        ],
    ]
];