<?php
use Illuminate\Validation\Rule;
/*
formrequest:
kan de naam zijn van een formrequest class
(genereren via php artisan make:request RequestName)

indien de waarde of requestname niet bestaat, worden de 'rules' waardes uit de array gelezen voor de validatie
*/

return [
    'name' => 'Products',
    //'formrequest' => 'ProductRequest',
    //'table' => 'brands',
    'model' => 'Product',
    'pk' => ['id'],
    'paginate' => 15,
    'default_sortfield' => 'id',
    'default_sortorder' => 'asc',
    'fields' => [
        [
        'name' => 'Id',
        'field_name' => 'id',
        'heading' => 1,
        ],
        [
        'name' => 'Prijs',
        'field_name' => 'price',
        'render_type' => 'textfield',   
        //'edit' => 2 //read only 
        'rules' => 'required|numeric'
        ],
        [
        'name' => 'Promo prijs',
        'field_name' => 'price_promo',
        'render_type' => 'textfield',   
        //'edit' => 2 //read only 
        'rules' => 'required|numeric'
        ],
        [
        'name' => 'tweede kans',
        'field_name' => 'secondhand',
        'field_type' => 'boolean',
        'render_type' => 'checkbox',   
        //'edit' => 2 //read only 
        'heading' => 1, 
        
        'rules' => 'required',
        'searchable' => 1,        
        ],
        [
        'name' => 'Stock',
        'field_name' => 'stock',
        'render_type' => 'textfield',   
        //'field_type' => 'boolean',
        //'edit' => 2 //read only 
        'rules' => 'required',
        'heading' => 1, 
        'searchable' => 1,                   
        ],
        [
        'name' => 'Titel',
        'field_name' => 'title',
        'render_type' => 'textfield',
        'heading' => 1,
        'rules' => 'required|unique:products,title,$id',
        'searchable' => 1,
        ],
        [
        'name' => 'Omschrijving',
        'field_name' => 'description',
        'render_type' => 'textarea',
        'rules' => 'required',
        'searchable' => 1,
        ],
        [
        'name' => 'Merk',
        'field_name' => 'brand_id',
        'render_type' => 'select2',
        'relation' => 'hasOne',
        'field_type' => 'query',
        'data_value' => 'SELECT id AS value, title AS label FROM brands ORDER BY title ASC',
        'heading' => 1,    
        'rules' => 'nullable',    
        'searchable' => 1,
        'render_type_search' => 'checkbox'
        ],      
        [
        'name' => 'Tags',
        'field_name' => 'tags',
        'render_type' => 'tags',
        'field_type' => 'tags',
        'heading' => 1,
        'rules' => '',
        'searchable' => 1, //todo: dit werkend krijgen
        ],
         [
            'name' => 'Reviews', //tab name
            'field_name' => 'reviews', //=relation name
            'render_type' => 'checkbox',
            'relation' => 'hasMany',
            'related_class' => 'App\Review',
            'related_field' => 'product_id', //veld waarmee de hasmany opgeslagen wordt
            'data_class' => 'App\Review', //class die de functie bevat waarmee je de lijst ophaalt
            'data_function' => 'all', // functie waarmee je de lijst ophaalt
            'data_value' => 'id', //lijst item id
            'data_label' => 'id', //lijst item value
            'searchable' => 1,
            'render_type_search' => 'select2'
        ],
    ],
    'media' => [
        [
            'name' => 'images', //collection name
            'title' => 'Images',
            'heading' => 1,
            'properties' => [
                'title' => [
                    'field_type' => 'textfield',   
                    'heading' => 1,
                ],
                'description' => [
                    'field_type' => 'textarea',   
                ]
            ]
        ],
        [
            'name' => 'detail',
            'title' => 'Detail images',
            'heading' => 1,            
        ]
    ],
    'tabs' => [
        [
            'field_name' => 'categories',
            'name' => 'Categories',
            'render_type' => 'checkbox',            
            'field_type' => 'belongsToMany',
            'data_class' => 'App\Category', //categories functie
            'data_function' => 'all', //categories functie
            'data_value' => 'id',
            'data_label' => 'title',
        ],
        [
            'field_name' => 'specifications',
            'name' => 'Specifications',
            'render_type' => 'checkbox',
            'field_type' => 'belongsToMany',
            'data_class' => 'App\Specifications', //categories functie
            'data_function' => 'all', //categories functie
            'data_value' => 'id',
            'data_label' => 'specification',
            'group_by_tag' => true,
            //'action' => 'ProductController@specificationsIndex',
        ],
        [
            'name' => 'Product specifications',
            'field_type' => 'custom',           
            'action' => 'ProductController@specificationsIndex',
        ],
        [
            'name' => 'Reviews', //tab name
            'field_name' => 'reviews',
            'field_type' => 'hasMany',
            'render_type' => 'checkbox',
            'related_class' => 'App\Review',
            'related_field' => 'product_id', //veld waarmee de hasmany opgeslagen wordt
            'data_class' => 'App\Review', //class die de functie bevat waarmee je de lijst ophaalt
            'data_function' => 'all', // functie waarmee je de lijst ophaalt
            'data_value' => 'id', //lijst item id
            'data_label' => 'id', //lijst item value
        ],
    ],
];