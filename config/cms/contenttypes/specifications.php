<?php
use Illuminate\Validation\Rule;
return [
    'name' => 'Specs',
    //'formrequest' => 'ProductRequest',
    //'table' => 'brands',
    'model' => 'Specifications',
    'pk' => ['id'],
    'paginate' => 15,
    'fields' => [
        [
            'name' => 'Id',
            'field_name' => 'id',
            'heading' => 1,            
        ],
        [
            'name' => 'Specification',
            'field_name' => 'specification',
            'render_type' => 'textfield',   
        //'edit' => 2 //read only 
            'rules' => 'required|unique:specifications,specification,$id',
            'heading' => 1,       
            'searchable' => 1,
        ],
        [
            'name' => 'Tags',
            'field_name' => 'tags',
            'render_type' => 'tags',
            'field_type' => 'tags',
            'heading' => 1,
            'rules' => '',
            'searchable' => 1,            
        ],    
    ],
    'media' => [
    ],
    'tabs' => [
        [
            'field_name' => 'products', //dit is de functie in de model class die de belongstomany definieert
            'name' => 'Products',
            'field_type' => 'belongsToMany',
            'render_type' => 'checkbox',            
            'data_class' => 'App\Product', //categories functie
            'data_function' => 'all', //categories functie
            'data_value' => 'id',
            'data_label' => 'title',
        ],
    ]
];