<?php

return [
    'name' => 'Categories',
    //'table' => 'brands',
    'model' => 'Category',
    'pk' => 'id',
    'fields' => [
        [
        'name' => 'Id',
        'field_name' => 'id',
        'heading' => 0,
        ],
        [
        'name' => 'titel',
        'field_name' => 'title',
        'heading' => 1,
        'rules' => 'required|unique:categories,title,$id',
        ],
        [
        'name' => 'Parent',
        'field_name' => 'parent_id',
        'render_type' => 'select',
        'field_type' => 'hierarchy',
        'data_value' => 'id',
        'data_label' => 'title',
        ],    
    ],
    'media' => [
        [
            'name' => 'images', //collection name
            'title' => 'Images',
            'heading' => 1,
        ],
    ],
    'tabs' => [
        [
            'field_name' => 'products', //dit is de functie in de model class die de belongstomany definieert
            'name' => 'Products',
            'field_type' => 'belongsToMany',
            'render_type' => 'checkbox',            
            'data_class' => 'App\Product', //categories functie
            'data_function' => 'all', //categories functie
            'data_value' => 'id',
            'data_label' => 'title',
        ],
    ]
];