<?php
return [
    'name' => 'Forms',
    //'table' => 'brands',
    'model' => 'Form',
    'model_namespace' => "App\\",
    'pk' => 'id',
    'default_sortfield' => 'title',
    'default_sortorder' => 'asc',
    'fields' => [
        [
        'name' => 'Id',
        'field_name' => 'id',
        'heading' => 1,
        ],
        [
        'name' => 'titel',
        'field_name' => 'title',
        'heading' => 1,
        'rules' => 'required|unique:forms,title,$id',
        ],  
        [
        'name' => 'Intro',
        'field_name' => 'intro',
        'render_type' => 'textarea',
        'rules' => 'required',
        ],  
        [   
        'name' => 'Message ok',
        'field_name' => 'message_ok',
        'render_type' => 'textarea',
        'rules' => 'required',
        ],
        [
        'name' => 'Verzender email',
        'field_name' => 'sender_email',
        'heading' => 1,
        'rules' => 'required',
        ],
        [
        'name' => 'Verzender naam',
        'field_name' => 'sender_name',
        'heading' => 1,
        'rules' => '',
        ],
        [   
        'name' => 'Ontvanger email adressen',
        'field_name' => 'receiver_emails',
        'render_type' => 'textarea',
        'rules' => 'sometimes',
        //'template' => 'cmsextra.form.receiver_emails' //cms formfield overschrijven met eigen versie
        ],
        [   
        'name' => 'Mail inhoud verzender',
        'field_name' => 'mailcontent_sender',
        'render_type' => 'textarea',
        'rules' => 'sometimes',
        ],
        [   
        'name' => 'Mail inhoud ontvanger',
        'field_name' => 'mailcontent_receiver',
        'render_type' => 'textarea',
        'rules' => 'sometimes',
        ],
    ],
    'tabs' => [
        [
            'name' => 'Fields',
            'field_type' => 'custom',           
            'action' => 'FormController@fieldsIndex',
        ],
         [
            'name' => 'Preview',
            'field_type' => 'custom',           
            'action' => 'FormController@preview',
        ]
        // [
        // 'name' => 'Fields', //tab name
        // 'field_name' => 'fields',
        // 'field_type' => 'hasMany',
        // 'related_class' => 'App\Field',
        // 'related_field' => 'form_id', //veld waarmee de hasmany opgeslagen wordt
        // 'data_class' => '', //class die de functie bevat waarmee je de lijst ophaalt
        // 'data_function' => 'fields', // functie waarmee je de lijst ophaalt
        // 'data_value' => 'id', //lijst item id
        // 'data_label' => 'title', //lijst item value
        // ]
    ]
];