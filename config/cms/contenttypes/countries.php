<?php
use Illuminate\Validation\Rule;
return [
    'name' => 'Countries',
    //'formrequest' => 'ProductRequest',
    //'table' => 'brands',
    'model' => 'Country',
    'pk' => ['id'],
    'paginate' => 15,
    'fields' => [
        [
        'name' => 'Id',
        'field_name' => 'id',
        'heading' => 1,            
        ],
        [
        'name' => 'Name',
        'field_name' => 'name',
        'render_type' => 'textfield',   
        //'edit' => 2 //read only 
        'rules' => 'required|unique:countries,name,$id',
        'heading' => 1,             
        'searchable' => 1,       
        ],
    ],
    'media' => [
    ],
    'tabs' => [
        [
            'name' => 'users',
            'field_name' => 'users',
            'field_type' => 'hasMany',
            'render_type' => 'checkbox',            
            'related_class' => 'App\User',
            'related_field' => 'country_id', //veld waarmee de hasmany opgeslagen wordt
            'data_class' => 'App\User', //class die de functie bevat waarmee je de lijst ophaalt
            'data_function' => 'all', // functie waarmee je de lijst ophaalt
            'data_value' => 'id', //lijst item id
            'data_label' => 'name', //lijst item value
        ],
    ]
];