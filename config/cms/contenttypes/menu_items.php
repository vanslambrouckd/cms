<?php
use Illuminate\Validation\Rule;
return [
    'name' => 'Menu items',
    //'formrequest' => 'ProductRequest',
    //'table' => 'brands',
    'model' => 'menu_items',
    'pk' => ['id'],
    'paginate' => 15,
    'fields' => [
        [
        'name' => 'Id',
        'field_name' => 'id',
        'heading' => 1,            
        ],
        [
        'name' => 'Title',
        'field_name' => 'title',
        'render_type' => 'textfield',   
        //'edit' => 2 //read only 
        'rules' => 'required',
        'heading' => 1,                
        'searchable' => 1,    
        ],
        [
        'name' => 'Action',
        'field_name' => 'action',
        'render_type' => 'textfield',   
        //'edit' => 2 //read only 
        'rules' => 'sometimes',
        'heading' => 1,                    
        ],
        [
        'name' => 'Url',
        'field_name' => 'url',
        'render_type' => 'textfield',   
        //'edit' => 2 //read only 
        'rules' => 'sometimes',
        'heading' => 1,                    
        'searchable' => 1,
        ],
        [
        'name' => 'Open in new window',
        'field_name' => 'new_window',
        'render_type' => 'checkbox',  
        'field_type' => 'boolean',  
        //'edit' => 2 //read only 
        'rules' => 'boolean',
        'heading' => 1,
        'searchable' => 1,
        ],
    ],
    'media' => [
    ]
];