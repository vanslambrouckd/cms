<?php
use Illuminate\Validation\Rule;
return [
    'name' => 'Tags',
    //'formrequest' => 'ProductRequest',
    //'table' => 'brands',
    'create_model' => false,
    'model' => 'Tag',
    'model_namespace' => "\Spatie\Tags\\",
    'default_sortfield' => 'order_column',
    'default_sortorder' => 'asc',
    'pk' => ['id'],
    'paginate' => 15,
    'fields' => [
        [
        'name' => 'Id',
        'field_name' => 'id',
        'heading' => 1,            
        ],
        [
        'name' => 'Name',
        'field_name' => 'name',
        'render_type' => 'textfield',   
        //'edit' => 2 //read only 
        'rules' => 'required',
        'heading' => 1,                    
        'searchable' => 1,
        ],
        [
        'name' => 'Order',
        'field_name' => 'order_column',
        'heading' => 1,       
        'rules' => 'required|numeric|min:0',     
        ],
        [
        'name' => 'Type',
        'field_name' => 'type',
        'render_type' => 'select2',
        'field_type' => 'json',
        'data_value' => 'specifications;specifications
            products;products'
        ,
        'heading' => 1,    
        'rules' => 'required',    
        'searchable' => 1,        
        ],        
    ],
    'media' => [
    ]
];