<?php
use Illuminate\Validation\Rule;
return [
    'name' => 'Reviews',
    //'formrequest' => 'ProductRequest',
    //'table' => 'brands',
    'model' => 'Review',
    'pk' => ['id'],
    'paginate' => 15,
    'fields' => [
        [
        'name' => 'Id',
        'field_name' => 'id',
        'heading' => 1,            
        ],
        [
        'name' => 'Omschrijving',
        'field_name' => 'description',
        'render_type' => 'textarea',
        'rules' => 'required',
        'heading' => 1,
        'rules' => 'required'
        ],
        [
        'name' => 'User',
        'field_name' => 'user_id',
        'render_type' => 'select',
        'relation' => 'hasOne',
        'field_type' => 'query',
        'data_value' => 'SELECT id AS value, name AS label FROM users ORDER BY name ASC',
        'heading' => 1,    
        'rules' => 'required',     
        'searchable' => 1,    
        'render_type_search' => 'checkbox'
        ],
        [      
        'name' => 'Product',
        'field_name' => 'product_id',
        'render_type' => 'select',
        'relation' => 'hasOne',
        'field_type' => 'query',
        'data_value' => 'SELECT id AS value, title AS label FROM products ORDER BY title ASC',
        'heading' => 1,    
        'rules' => 'required',            
        'searchable' => 1,       
        //'render_type_search' => 'select'
        ],  
    ],
    'media' => [
    ]
];