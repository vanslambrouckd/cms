<?php
use Illuminate\Validation\Rule;
return [
    'name' => 'Users',
    //'formrequest' => 'ProductRequest',
    //'table' => 'brands',
    'model' => 'User',
    'pk' => ['id'],
    'paginate' => 15,
    'fields' => [
        [
        'name' => 'Id',
        'field_name' => 'id',
        'heading' => 1,            
        ],
        [
        'name' => 'Name',
        'field_name' => 'name',
        'render_type' => 'textfield',   
        //'edit' => 2 //read only 
        'rules' => 'required',
        'heading' => 1,                    
        'searchable' => 1,
        ],
        [
        'name' => 'Email',
        'field_name' => 'email',
        'render_type' => 'textfield',   
        //'edit' => 2 //read only 
        'rules' => 'required|unique:users,email,$id',
        'heading' => 1,     
        'searchable' => 1,

        ],
        [
        'name' => 'Password',
        'field_name' => 'password',
        'render_type' => 'password',   
        //'edit' => 2 //read only 
        'rules' => 'required',
        ],
        [
        'name' => 'Country',
        'field_name' => 'country_id',
        'render_type' => 'select2',
        'relation' => 'hasOne',
        'field_type' => 'query',
        'data_value' => 'SELECT id AS value, name AS label FROM countries ORDER BY name ASC',
        'heading' => 1,    
        'rules' => 'required',            
        'searchable' => 1,
        'render_type_search' => 'checkbox',
        ],  
        [
            'name' => 'Reviews',
            'field_name' => 'reviews',
            //'field_type' => 'hasMany',
            'relation' => 'hasMany',            
            'render_type' => 'checkbox',            
            'related_class' => 'App\Review',
            'related_field' => 'user_id', //veld waarmee de hasmany opgeslagen wordt
            'data_class' => 'App\Review', //class die de functie bevat waarmee je de lijst ophaalt
            'data_function' => 'all', // functie waarmee je de lijst ophaalt
            'data_value' => 'id', //lijst item id
            'data_label' => 'id', //lijst item value,
            'searchable' => 1,
            'render_type_search' => 'select2'
        ]             
    ],
    'media' => [
    ],
    'tabs' => [
        [
            'name' => 'Reviews',
            'field_name' => 'reviews',
            'field_type' => 'hasMany',
            'render_type' => 'checkbox',            
            'related_class' => 'App\Review',
            'related_field' => 'user_id', //veld waarmee de hasmany opgeslagen wordt
            'data_class' => 'App\Review', //class die de functie bevat waarmee je de lijst ophaalt
            'data_function' => 'all', // functie waarmee je de lijst ophaalt
            'data_value' => 'id', //lijst item id
            'data_label' => 'id', //lijst item value,
        ]        
    ]
];