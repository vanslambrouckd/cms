<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->unique();
            $table->string('subject')->nullable();
            $table->string('sender_email')->nullable();
            $table->string('sender_name')->nullable();
            $table->text('receiver_emails')->nullable();
            $table->text('mailcontent_sender')->nullable();
            $table->text('mailcontent_receiver')->nullable();
            $table->text('intro')->nullable();
            $table->text('message_ok')->nullable();
            $table->timestamps();
        });

        Schema::create('formfields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->unique();
            $table->string('field_name')->unique();
            $table->enum('render_type', ['textfield', 'textarea', 'email', 'select', 'select2', 'checkbox'])->default('textfield');
            $table->enum('field_type', ['json', 'query', 'function'])->default('json')->nullable();
            $table->text('data_value')->nullable();
            $table->integer('form_id')->unsigned()->nullable();
            $table->foreign('form_id')->references('id')->on('forms')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formfields');
        Schema::dropIfExists('forms');
    }
}
