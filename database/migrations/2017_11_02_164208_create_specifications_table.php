<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('specification');
            $table->timestamps();
        });

        Schema::create('product_specifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('specifications_id');
            $table->string('value')->nullable();
            //$table->primary(['id']);
            $table->unique(['product_id', 'specifications_id']);
            // $table->primary(['product_id', 'specifications_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specifications');
        Schema::dropIfExists('product_specifications');
    }
}
