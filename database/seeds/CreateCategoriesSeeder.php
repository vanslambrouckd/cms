<?php

use Illuminate\Database\Seeder;
use App\Category;

class CreateCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //factory(App\Category::class, 20)->create();
        $roots = [
            [
                'title' => 'Telefonie & tablets',
                'children' => [
                    [
                        'title' => 'Mobiele telefonie',
                        'children' => [
                        ]
                    ],
                    [
                        'title' => 'Tablets & accessoires',
                        'children' => [
                        ]
                    ]
                ]
            ],
            [
                'title' => 'Computer & netwerk',
                'children' => [
                ]
            ],
            [
                'title' => 'TV & Audio',
                'children' => [
                ]
            ],
            [
                'title' => 'Witgoed & huishouden',
                'children' => [
                ]
            ],
            [
                'title' => 'Koken & wonen',
                'children' => [
                ]
            ],
            [
                'title' => 'Foto & video',
                'children' => [
                ]
            ],
            [
                'title' => 'Navigatie & reizen',
                'children' => [
                ]
            ],
        ];  
        foreach($roots as $item) {
            $el = Category::create(['title' => $item['title']]);
            if (!empty($item['children'])) {
                foreach($item['children'] as $child) {
                    $el_sub = Category::create(['title' => $child['title']]);
                    $el_sub->makeChildOf($el);
                }
            }
        }
        // $root = Category::create(['title' => 'Telefonie & tablets']);

        // $root1 = Category::create(['title' => 'R1']);
        // $root2 = Category::create(['title' => 'R2']);

        // $child1 = Category::create(['title' => 'C1']);
        // $child2 = Category::create(['title' => 'C2']);

        // $child1->makeChildOf($root1);
        // $child2->makeChildOf($root2);

        // $root1->children()->get(); // <- returns $child1
        // $root2->children()->get(); // <- returns $child2
    }
}
