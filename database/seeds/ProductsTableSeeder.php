<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    private function createBrands() 
    {
        $brands = [];
        $brands[] = ['title' => 'Apple'];
        $brands[] = ['title' => 'Samsung'];
        $brands[] = ['title' => 'Motorola'];
        $brands[] = ['title' => 'Nokia'];
        $brands[] = ['title' => 'Huawei'];
        foreach($brands as $brand) {
            DB::table('brands')->insert($brand);
        }
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createBrands();

        //
        $data = [];
        $data['title'] = 'Apple iPhone 6 32GB Grijs';
        $data['price'] = 395;
        $data['description'] = "Een goede camera en een gebruiksvriendelijk besturingssysteem in een handzame behuizing, dat is Apple iPhone 6. Ten opzichte van zijn voorganger, de iPhone 5s, is het scherm iets gegroeid naar 4,7 inch. Hierdoor kijk je nog prettiger foto's en video's op je telefoon zonder dat je het toestel niet meer met 1 hand kunt bedienen.<br /><br />
        Standaard meegeleverd:<br /><br />

        Lightning naar usb kabel<br />
        Earpods headset<br />
        Oplader<br />
        Handleiding";
        $data['brand_id'] = 1;
        DB::table('products')->insert($data);

        $data = [];
        $data['title'] = 'SAMSUNG GALAXY J5 (2017) DUAL SIM ZWART';
        $data['price'] = 249;
        $data['description'] = "De Samsung Galaxy J5 (2017) is een stevig toestel met een stijlvolle uitstraling dankzij de metalen behuizing. Met deze robuuste smartphone maak je scherpe foto's. Het toestel heeft zowel aan de voorkant als aan de achterkant een 13 megapixel camera met flitser. Zo leg je je omgeving en jezelf mooi vast, ook bij weinig licht. De telefoon beschikt over een intern geheugen van 16 GB om je foto's op te slaan. Dit geheugen is tot 256 GB uit te breiden met een geheugenkaart. Of plaats een tweede simkaart zodat je op 2 telefoonnummers bereikbaar bent. Je ontgrendelt het toestel razendsnel via de vingerafdrukscanner, zodat je gemakkelijk belt, foto's maakt of een appje stuurt.<br /><br />
        Standaard meegeleverd:<br /><br />

        Lightning naar usb kabel<br />
        Earpods headset<br />
        Oplader<br />
        Handleiding";
        $data['brand_id'] = 2;
        DB::table('products')->insert($data);

        factory(App\Product::class, 10)->create();

        $categoryIds = App\Category::pluck('id')->all(); // returns an array of all ids in the Lessons table

        for ($i = 1; $i < 11; $i++) {
            $data = ['product_id' => $i, 'category_id' => $categoryIds[array_rand($categoryIds)]];
            DB::table('category_product')->insert($data);
        }
    }
}
