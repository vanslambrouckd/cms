<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;
use App\Form;

class FormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //factory(App\Review::class, 20)->create();
        $faker = Faker::create();
        //$user_ids = User::all()->pluck('id')->toArray();
        
        $data[] = [
            'title' => 'Form1',
        ];

        foreach($data as $item) {
            $spec = Form::create([
                'title' => $item['title']
            ]);
        }
    }
}
