<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;
use App\Review;

class ReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //factory(App\Review::class, 20)->create();
        $faker = Faker::create();
        $user_ids = User::all()->pluck('id')->toArray();
        foreach(range(1, 50) as $index)
        {
            Review::create([
                'description' => $faker->realText(500),
                'user_id' => $faker->randomElement($user_ids)
            ]);
        }
    }
}
