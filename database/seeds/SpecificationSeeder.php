<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;
use App\Specifications;
use Spatie\Tags;
class SpecificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //factory(App\Review::class, 20)->create();
        $faker = Faker::create();
        //$user_ids = User::all()->pluck('id')->toArray();
        
        $data[] = [
            'specification' => 'Fabrikantcode',
            'tags' => ['Product']
        ];

        $data[] = [
            'specification' => 'Artikelnummer',
            'tags' => ['Product']
        ];

        $data[] = [
            'specification' => 'Garantie',
            'tags' => ['Product']
        ];

        $data[] = [
            'specification' => 'Garantietype',
            'tags' => ['Product']
        ];

        $data[] = [
            'specification' => 'Gewicht',
            'tags' => ['Fysieke eigenschappen']
        ];
        $data[] = [
            'specification' => 'Hoogte',
            'tags' => ['Fysieke eigenschappen']
        ];
        $data[] = [
            'specification' => 'Breedte',
            'tags' => ['Fysieke eigenschappen']
        ];
        $data[] = [
            'specification' => 'Versie IOS',
            'tags' => ['Algemene eigenschappen']
        ];
        $data[] = [
            'specification' => 'Besturingssysteem',
            'tags' => ['Algemene eigenschappen']
        ];
        $data[] = [
            'specification' => 'Introductiedatum',
            'tags' => ['Algemene eigenschappen']
        ];

        $data[] = [
            'specification' => 'Geheugenkaartlezer',
            'tags' => ['Geheugen']
        ];

        $data[] = [
            'specification' => 'Opslagcapaciteit',
            'tags' => ['RAM-Geheugen']
        ];

        $data[] = [
            'specification' => 'Geheugenkaartlezer',
            'tags' => ['Geheugen']
        ];
        foreach($data as $item) {
            $spec = Specifications::create([
                'specification' => $item['specification']
            ]);
            if (!empty($item['tags'])) {
                foreach($item['tags'] as $tag_title) {
                    //$spec->attach(\Spatie\Tags\Tag::findOrCreate('test'););
                    $tag = \Spatie\Tags\Tag::findOrCreate($tag_title, 'specifications');                    
                }
                $spec->syncTagsWithType($item['tags'], 'specifications');  
            }
        }
    }
}
