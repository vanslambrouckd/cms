<?php
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Country;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //factory(App\User::class, 20)->create();
        $faker = Faker::create();
        $country_ids = Country::all()->pluck('id')->toArray();
        foreach(range(1, 100) as $index)
        {
            $data = [
                'country_id' => $faker->randomElement($country_ids),
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'password' => $faker->password,
                //'password' => $password ?: $password = bcrypt('secret'),
                'remember_token' => str_random(10),
            ];
            User::create($data);
        }
    }
}
