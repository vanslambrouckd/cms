<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(CountriesSeeder::class);
         $this->call(CreateCategoriesSeeder::class);
         $this->call(ProductsTableSeeder::class);
         $this->call(UserSeeder::class);
         $this->call(ReviewSeeder::class);
         $this->call(SpecificationSeeder::class);
         $this->call(FormSeeder::class);
    }
}
