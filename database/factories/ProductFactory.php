<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        //
        'title' => $faker->unique()->word,
        'price' => $faker->randomNumber(2),
        'description' => $faker->paragraph()
    ];
});
